CustomerName -

Thank you for the feedback!  I've opened this issue to track this request:

https://gitlab.com/liscioapps/listmeapp/issues/XX

Feel free to upvote it by clicking the "thumbs up" or add additional comments about this idea and how it will help your workflow.

Thanks for using listMe!


--

Brendan O'Leary
listMe Customer Success