var braintree = require("braintree");

module.exports = braintree.connect({
    environment:  braintree.Environment.Production,
    merchantId:   process.env.btmerchantId,
    publicKey:    process.env.btpublicKey,
    privateKey:   process.env.btprivateKey
});