var request = require('request');

module.exports = {
    send: (rURL, body, inchannel) => {
        if (!(body.text)) {
            var txt = body;
            body = {};
            body.text = txt;
        }
        if (inchannel) {
            body.response_type = "in_channel"
        }
            request.post({
            url: rURL,
            json: true,
            body: body
        });
    }
    
}