/**
 * Created by Brendan on 2/10/19.
 */
const uuidv1 = require('uuid/v1');
var request = require('request');
var braintree = require("braintree");
var gateway = require('./braintree.js');

module.exports = function(app, redis){
    var databaseClient = redis.createClient(process.env.REDIS_URL);

    app.post('/api/beta-accept', function(req, res) {
        var code = req.body.code;
        visit.event("Beta", "Code Accepted", code).send();
        if (code) {
            databaseClient.multi()
                .set("beta:code:" + code, "USED")
                .hmset("beta:code:" + code + ":details", {
                    "used": "USED",
                    "useddt": moment.tz("America/New_York").format()
                })
                .exec();

            res.send("OK");
        } else {
            res.sendStatus(500);
        }
    });

    app.get('/api/admin/betainfo', function(req, res) {
        if (req.header('Authorization') == process.env.clientSecret) {
            databaseClient.keys("beta*details", function(err, results) {
                if (err) { console.error(err); res.sendStatus(500); } else {
                    var multi = databaseClient.multi();
                    for (i = 0; i < results.length; i++) {
                        multi.hgetall(results[i]);
                    }

                    multi.exec(function(err2, allteams) {
                        if (err2) { console.error(err2); res.sendStatus(500); } else {
                            res.json(allteams);
                        }
                    })
                }
            })
        } else {
            res.sendStatus(401);
        }

    });

    app.post('/api/admin/betacode', function(req, res) {
        databaseClient.keys("beta:code:*", function(err, results) {
            if (err) { console.error(err); res.sendStatus(500); } else {
                var newId = uuidv1().toString();
                newId = newId.substring(0, newId.indexOf("-")).toUpperCase();
                if (results.indexOf(newId) > 0) {
                    res.sendStatus(500);
                } else {
                    var user = (req.body.user || "unk");
                    var name = (req.body.name || "unk");
                    var email = (req.body.email || "email");
                    var sent = "Not sent";
                    if (user == "Twitter" || user == "Facebook") {
                        sent = "via " + user;
                    }
                    databaseClient.multi()
                        .set("beta:code:" + newId, "Not used")
                        .hmset("beta:code:" + newId + ":details", "code", newId)
                        .hmset("beta:code:" + newId + ":details", "user", user)
                        .hmset("beta:code:" + newId + ":details", "name", name)
                        .hmset("beta:code:" + newId + ":details", "email", email)
                        .hmset("beta:code:" + newId + ":details", "sent", sent)
                        .hmset("beta:code:" + newId + ":details", "used", "Not used")
                        .exec();

                    emailclient.sendEmailWithTemplate({
                        "From": "support@listme.chat",
                        "To": req.body.email,
                        "TemplateId": 982284,
                        "TemplateModel": {
                            "product_name": "listMe",
                            "name": name,
                            "sender_name": "listMe Support",
                            "invitecode": newId,
                            "product_address_line1": "P.O. Box 3704",
                            "product_address_line2": "Crofton, MD 21114",
                            "action_url": "http://listme.chat/beta",
                            "helpurl": "https://listme.chat/help"
                        }
                    }, function(error, success) {
                        if(error) {
                            console.error("Unable to send beta via postmark: " + error.message);
                        } else {
                            databaseClient.hmset("beta:code:" + newId + ":details", "sent", "Sent!");
                        }
                    });

                    res.send(newId);
                }
            }
        });
});
}