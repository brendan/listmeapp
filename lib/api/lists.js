const auth      = require('../helpers/auth')

module.exports = function Constructor(slacklist) {
    const debug     = slacklist.debug;

    this.getAll = (req, res) => {
        if (debug) { console.log(req.body); }
        slacklist.apiLog("GET ALL LISTS", "lists.get", req)
        if (auth.intraapp(req)) {
            slacklist.databaseClient.smembers("listoflists", function(err, listoflists) {
                if (!err) {
                    res.send(listoflists);
                }
                else {
                    res.status(500).send({ error: err });
                }
            })
        }
        else {
            res.sendStatus(401);
        }
    }
}