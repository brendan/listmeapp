const auth      = require('../helpers/auth')
const dt        = require('../helpers/datetime')

module.exports = function Constructor(slacklist) {
    const debug     = slacklist.debug;
    
    this.getUser = (req, res) => {
        if (debug) { console.log(req.body); }
        slacklist.apiLog("GET USER", "teams.users.get", req)
        var team = 'team:' + req.params.team_id;
        if (auth.intraapp(req)) { 
            slacklist.databaseClient.sismember("listofteams", team, function(err, reply) {
                if (err) { console.error(err); }
                if (reply) {
                    slacklist.databaseClient.smembers(team + ":users", function(err, listoflists) {
                        if (!err) {
                            res.send({
                                team: team,
                                user_count: listoflists.length,
                                users: listoflists
                            });
                        }
                        else {
                            res.status(500).send({ error: err });
                        }
                    })
                }
                else {
                    res.sendStatus(404);
                }
            });
        } else { res.sendStatus(401); }
    }

    this.addUser = (req, res) => {
        if (debug) { console.log(req.body); }
        slacklist.apiLog("POST TEAM USER", "teams.users.post", req)
        var team = 'team:' + req.params.team_id;
        var today = dt.gettoday();
        var thismon = dt.gettoday("month");
        if (auth.intraapp(req)) {
            if (req.body.user_id) {
                slacklist.databaseClient.sismember("listofteams", team, function(err, reply) {
                    if (err) { console.error(err); }
                    if (reply) {
                        slacklist.databaseClient.sismember(team + ":users:firstmsg", req.body.user_id, function (err2, reply2) {
                                if (reply2 == 0) {
                                    slacklist.databaseClient.sadd(team + ":users:firstmsg", req.body.user_id);
                                    slacklist.request.post( {
                                    url: req.body.response_url,
                                    json: true,
                                    body: {
                                        text: "Hey there - welcome to listMe! listMe works best when utilized by the whole team. Below is a link to share with your team members. Type /list help at any time to see all the available commands. https://www.youtube.com/watch?v=Db-KKHUp7oo"
                                    }
                                }, function(e, rr) {
                                    if (e) { console.error(e); }
                                });
                            }
                            });
                        slacklist.databaseClient.multi()
                            .sadd(team + ":users", req.body.user_id)
                            .sadd("users:allusers", team + "_" + req.body.user_id)
                            .sadd("stats:DailyActiveUsers:" + today, team + "_" + req.body.user_id)
                            .sadd("stats:MonthlyActiveUsers:" + thismon, team + "_" + req.body.user_id)
                            .sadd("stats:MonthlyActiveUsersByTeam:" + team + ":" + thismon, req.body.user_id)
                            .exec();
                        res.sendStatus(200); }
                    else { res.sendStatus(404); }
                });
            }
            else {res.sendStatus(400);}
        } else { res.sendStatus(401); }
    }
}