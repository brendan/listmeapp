/**
 * Created by Brendan on 8/18/16.
 */
var request = require('request');
var braintree = require("braintree");
var gateway = require('../braintree.js');
var moment = require('moment-timezone');
var postmark = require("postmark");
var emailclient = new postmark.Client("c26c930e-781a-465b-869b-232cc8dbedd5");
var http = 'https://';
if ((process.env.NODE_ENV || 'development') === 'development') {
    http = 'http://';
}

module.exports = function(app, redis){
    var databaseClient = redis.createClient(process.env.REDIS_URL);

    /**
     * @api {get} /admin/teaminfo Team info
     * @apiName GetTeamInfo
     * @apiGroup Admin
     *
     * @apiVersion 1.0.2
     * 
     * @apiUse StdAuth
     * @apiDescription Get team information for all teams
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * [
     *   {
     *       "paid": "false",
     *       "slackID": "TA08RSMHD",
     *       "slackTeamName": "citycatvetsgroup",
     *       "email": "unk"
     *   },
     *   {
     *       "paid": "false",
     *       "slackID": "TDPPGSECW",
     *       "slackTeamName": "Echtzeit",
     *       "email": "unk"
     *   },
     *   ...
     *  ]
     */
    app.get('/api/admin/teaminfo', function(req, res) {
        if (req.header('Authorization') == process.env.clientSecret) {
            databaseClient.keys("team*details", function(err, results) {
                if (err) { console.error(err); res.sendStatus(500); } else {
                    var multi = databaseClient.multi();
                    for (i = 0; i < results.length; i++) {
                        multi.hgetall(results[i]);
                    }

                    multi.exec(function(err2, allteams) {
                        if (err2) { console.error(err2); res.sendStatus(500); } else {
                            res.json(allteams);
                        }
                    })
                }
            })
        } else {
            res.sendStatus(401);
        }

    });

    app.get('/api/admin/stats', function(req, res) {
        var today = gettoday();
        var thismon = gettoday("month");
        if (req.header('Authorization') == process.env.clientSecret) {
            databaseClient.multi()
                .scard("stats:DailyActiveUsers:" + today)
                .zrevrange("stats:items:alltimescore", 0, 200, "WITHSCORES")
                .scard("stats:MonthlyActiveUsers:" + thismon)
                .lrange("stats:items:runninglist", 0, 99)
                .exec(function (err, results) {
                    if (err) { console.log(err); res.sendStatus(500)} else {
                        var stats = {
                            DAU: results[0],
                            itemlist: results[1],
                            MAU: results[2],
                            lList: results[3]
                        };
                        res.json(stats);
                    }
                })
        }
        else {
            res.sendStatus(401);
        }
    });

    app.get('/api/admin/stats/bymonth', function(req, res) {
        var thismon = gettoday("month");
        if (req.query.thismon) {
            thismon = req.query.thismon;
        }
        if (req.header('Authorization') == process.env.clientSecret) {
            databaseClient.keys("stats:MonthlyActiveUsers:*", function(err, results) {
                if (err) { console.error(err); res.sendStatus(500); } else {
                    var multi = databaseClient.multi();
                    results.sort();
                    for (i = 0; i < results.length; i++) {
                        multi.scard(results[i]);
                    }
    
                    multi.exec(function(err2, alldays) {
                        if (err2) { console.error(err2); res.sendStatus(500); } else {
                            let ret = [];
                            for (i = 0; i < results.length; i++) {
                                ret.push({
                                    month: results[i].replace("stats:MonthlyActiveUsers:", "").replace(":", "-"),
                                    count: alldays[i]
                                })
                            }
                            res.json(ret);
                        }
                    })
                }
            });   
        }
        else {
            res.sendStatus(401);
        }
    });

    /**
     * @api {get} /admin/stats/byteam Stats by Team
     * @apiName ByTeamStats
     * @apiGroup Admin
     * 
     * @apiVersion 1.0.2
     *
     * @apiUse StdAuth
     * @apiDescription Get team stats for a given month
     * @apiParam {String} [thismon=CurrentMonth] The month to search in the form `2018:09`
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * [
     *   ...
     * ]
     */
    app.get('/api/admin/stats/byteam', function(req, res) {
        var thismon = gettoday("month");
        if (req.query.thismon) {
            thismon = req.query.thismon;
        }
        if (req.header('Authorization') == process.env.clientSecret) {
            databaseClient.keys("stats:MonthlyActiveUsersByTeam:team:*:" + thismon, function(err, results) {
                if (err) { console.error(err); res.sendStatus(500); } else {
                    var multi = databaseClient.multi();
                    for (i = 0; i < results.length; i++) {
                        multi.hgetall(results[i].replace("stats:MonthlyActiveUsersByTeam:", "").replace(thismon, "details"));
                        multi.scard(results[i]);
                    }

                    multi.exec(function(err2, allteams) {
                        if (err2) { console.error(err2); res.sendStatus(500); } else {
                            let newarr = [];
                            for (i = 0; i < allteams.length; i++) {
                                if (typeof allteams[i] === 'object') {
                                    var newobj = allteams[i];
                                    newobj.usercount = allteams[i+1];
                                    newarr.push(newobj);
                                }
                            }
                            res.json(newarr);
                        }
                    })
                }
            });
        }
        else {
            res.sendStatus(401);
        }
    });

    app.get('/api/admin/dau', function(req, res) {
        var today = gettoday();
        var thismon = gettoday("month");
        if (req.header('Authorization') == process.env.clientSecret) {
            databaseClient.multi()
                .get("stats:messages:" + today)
                .scard("stats:DailyActiveUsers:" + today)
                .smembers("stats:DailyActiveTeams:" + today)
                .scard("stats:MonthlyActiveUsers:" + thismon)
                .exec(function (err, results) {
                    if (err) { console.log(err); res.sendStatus(500)} else {
                        var activeteams = results[2];
                        
                        request.get({
                            url: http + req.get('host') + '/api/admin/teaminfo',
                            headers: { Authorization: process.env.clientSecret}
                        }, function(err2, teaminfo) {
                           if (err2) {console.log(err2); res.sendStatus(500)} else {
                               var teamdetails = JSON.parse(teaminfo.body);
                               for (i = 0; i < teamdetails.length; i++) {
                                   if (activeteams.indexOf(teamdetails[i].slackID) > -1 ){
                                       teamdetails[i].activetoday = "Yes";
                                   } else { teamdetails[i].activetoday = "No"; }
                               }
                               teamdetails.sort(SortByActiveToday);
                               res.json({
                                   DAU: results[1],
                                   messages: results[0],
                                   activeteams: teamdetails,
                                   MAU: results[3]
                               });
                           }
                        });
                    }
                })
        }
        else {
            res.sendStatus(401);
        }
    });

    app.get('/api/admin/opentickets', function(req, res) {
        if (req.header('Authorization') == process.env.clientSecret) {
            request.get({
                url: "https://gitlab.com/api/v4/projects/liscioapps%2Flistmeapp/issues",
                json: true,
                qs: {
                    state: 'opened',
                    private_token: process.env.GITLAB_TOKEN
                }
            }, function(err,httpResponse,body){ 
                res.json(body);
             });
        }
        else {
            res.sendStatus(401);
        }
    });
};

function notimplemented(res) {
    res.status(500).send({ error: "Not yet implemented"});
}

function SortByActiveToday(a, b){
    var aName = a.activetoday.toLowerCase();
    var bName = b.activetoday.toLowerCase();
    return ((aName < bName) ? 1 : ((aName > bName) ? -1 : 0));
}

function gettoday(type) {
    switch (type) {
        case "month":
            return moment.tz("America/New_York").format("YYYY:MM");
        default:
            return moment.tz("America/New_York").format("YYYY:MM:DD");
    }
}