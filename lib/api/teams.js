const auth      = require('../helpers/auth')

module.exports = function Construtor(slacklist) {
    const debug     = slacklist.debug;

    this.getTeams = (req, res) => {
        if (debug) { console.log(req.body); }
            slacklist.apiLog("GET ALL TEAMS", "teams.getAll", req)
            if (auth.intraapp(req)) {
                var listitems = [];
                console.log("TRUCKS")
                slacklist.databaseClient.smembers("listofteams", function(err, listoflists) {
                    console.log("1")
                    if (!err) {
                        console.log("2")
                        res.send(listoflists);
                    }
                    else {
                        console.log("3")
                        res.status(500).send({ error: err });
                    }
                })
            }
            else {
                console.log("4")
                res.sendStatus(401);
            }
    }

    this.getSingleTeam = (req, res) => {
        if (debug) { console.log(req.body); }
        slacklist.apiLog("GET TEAM", "teams.get", req)
        if (auth.intraapp(req)) {
            var team = 'team:' + req.params.team_id;
            slacklist.databaseClient.sismember("listofteams", team, function(err, reply) {
                if (err) { console.error(err); }
                if (reply) {
                    slacklist.databaseClient.hgetall(team, function(err, reply) {
                        res.send({
                            team_id: team,
                            details: reply});
                    })
                }
                else {
                    res.sendStatus(404);
                }
            });
        }
        else {
            res.sendStatus(401);
        }
    }

    this.addTeam = (req, res) => {
        var domain = req.body.team_domain;
        var email = req.body.email;
        if (debug) { console.log(req.body); }
        slacklist.apiLog("POST TEAMS", "teams.post", req)
        if (auth.intraapp(req)) {
            if (req.body.team_id) {
                var newTeam = {};
                newTeam.team_id = 'team:' + req.body.team_id;
                var details = newTeam.team_id + ":details";

                slacklist.databaseClient.sadd("listofteams", newTeam.team_id);

                slacklist.databaseClient.keys(details, function(err, kobj) {
                    if (err) { res.sendStatus(500) }
                    if (kobj.length > 0) {
                        res.sendStatus(200);
                    }
                    else
                    {
                        slacklist.databaseClient.hmset(details, {
                            "paid": (req.body.paid || "false"),
                            "slackID": req.body.team_id,
                            "slackTeamName": req.body.team_domain,
                            "email": (req.body.email || "unk")
                        }, function(errdb, rrr) {
                            slacklist.request.post({
                                url: 'https://hooks.slack.com/services/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',
                                json: true,
                                body: {
                                attachments: [
                                    {
                                    fallback: "WOOO!  New team added: " + req.body.team_domain,
                                    color: "#36a64f",
                                    pretext: "WOOO!  New team added: " + req.body.team_domain,
                                    text: JSON.stringify(req.body.identity)
                                    }
                                ]
                                }
                            });
                        });
                        res.sendStatus(200);
                    }
                });
            }
            else { res.status(500).send({ error: "Bad request"}); }
        }
        else {
            var detailskey = 'team:' + req.body.team_id + ":details";
            slacklist.databaseClient.keys(detailskey, function(err, kobj) {
                if (err) { res.sendStatus(500) } else {
                  if (domain) { databaseClient.hmset(detailskey, "slackTeamName", req.body.team_domain); }
                    if (email) { databaseClient.hmset(detailskey, "email", email)}
                }
            });
            res.sendStatus(401);
        }
    }

    this.deleteTeam = (req, res) => {
        if (debug) { console.log(req.body); }
        slacklist.apiLog("DELETE TEAM", "teams.delete", req)
        if (auth.intraapp(req)) {
            return res.status(500).send({ error: "Not yet implemented"});
            var team = 'team:' + req.params.team_id;
            databaseClient.sismember("listofteams", team, function(err, reply) {
                if (err) { console.error(err); }
                if (reply) {
                    databaseClient.srem("listofteams", team, function(err, reply) {
                        res.sendStatus(204);
                    })
                }
                else {
                    res.sendStatus(404);
                }
            });
        }
        else {
            res.sendStatus(401);
        }
    }

    this.getTeamDetails = (req, res) => {
        if (debug) { console.log(req.body); }
        slacklist.apiLog("GET TEAM DETAILS", "teams.get.details", req)
        if (auth.intraapp(req)) {
            var team = 'team:' + req.params.team_id;

            var multi = slacklist.databaseClient.multi();

            let statmonth = "stats:MonthlyActiveUsersByTeam:" + team

            multi.smembers(team + ":paywall:months");
            multi.sismember("paywall:PAYING", req.params.team_id);
            multi.hgetall(team + ":details");
            multi.smembers(team + ":users");
            multi.keys(statmonth + "*");
            multi.get(team + ":plan");

            multi.exec(function(err, results) {
                mau = results[4].map(function (month) {
                    return month.replace(statmonth + ":", "");
                })
                mau.sort().reverse();

                res.json(
                    {
                        details: results[2],
                        users: results[3],
                        paying: results[1] == 1,
                        plan: results[5],
                        history: {
                            paywallMonths: results[0],
                            activeMonths: mau
                        }
                    }
                );
            });
        }
        else {
            res.sendStatus(401);
        }
    }

    this.getTeamPlan = (req, res) => {
        if (debug) { console.log(req.body) }
        slacklist.apiLog("GET TEAM PLAN", "teams.get.plan", req)
        if (auth.intraapp(req)) {
            slacklist.databaseClient.get(`team:${req.params.team_id}:plan`, function(err, dbOut) {
                if (err) { res.sendStatus(500); }
                res.json({ plan: (dbOut || "basic") });
            })
        } else {
            res.sendStatus(401);
        }
    }
}