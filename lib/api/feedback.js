const auth      = require('../helpers/auth');
const uuidv1    = require('uuid/v1');

module.exports = function Construtor(slacklist) {
    const debug     = slacklist.debug;

    this.sendFeedback = (req, res) => {
        if (debug) { console.log(req.body); }
        slacklist.apiLog("POST FEEDBACK", "feedback.post", req)
        var feedback = req.body.name;
        var fID = uuidv1();
        fID = fID.substring(0, fID.indexOf("-"));

        if (feedback) {
            var multi = slacklist.databaseClient.multi();

            multi.lpush("stats:feedback", feedback);
            multi.hset("stats:feedback:" + fID, "details", feedback);
            if (req.body.user) { multi.hset("stats:feedback:" + fID, "user", req.body.user); }

            multi.exec();

            slacklist.request.post({
                    url: req.body.response_url,
                    json: true,
                    body: {
                        text: "Thanks for the feedback!"
                    }
                });

            slacklist.logger.info(`#️⃣  Send feedback to Slack`, 
                { caller: `feedback.slack`, 
                api: false, hostname: req.hostname, path: req.path, 
                details: { type: 'general', file: "feedback.js"} 
            });
            slacklist.request.post({
                url: "https://hooks.slack.com/services/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",
                json: true,
                body: {
                    text: "Feedback from app (ID " + fID + "): " + feedback
                }
            });


            if (process.env.GITLAB_TOKEN) {
                if (debug) { console.log("sending issue to GitLab"); }
                slacklist.logger.info(`🦊 Send feedback to GitLab`, 
                    { caller: `feedback.gitlab`, 
                    api: false, hostname: req.hostname, path: req.path, 
                    details: { type: 'general', file: "feedback.js"} 
                });
                
                slacklist.request.post({
                    url: "https://gitlab.com/api/v4/projects/liscioapps%2Flistmeapp/issues/",
                    json: true,
                    qs: {
                        title: feedback + " (ID " + fID + ")",
                        labels: 'In-app Feedback',
                        private_token: process.env.GITLAB_TOKEN
                    }
                }, function(err,httpResponse,body){ 
                    if (debug) { console.log(body.web_url); }
                    if (err) { slacklist.logger.error(err.message) }
                    slacklist.request.post({
                        url: req.body.response_url,
                        json: true,
                        body: {
                            text: "To provide more details (or follow the issue) visit " + body.web_url
                        }
                    });
                 });
            }
            

            res.sendStatus(200);
        } else {
            res.sendStatus(400);
        }
    }
}