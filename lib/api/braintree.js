/**
 * Created by Brendan on 8/18/16.
 */
var request = require('request');
var gateway = require('../braintree.js');
var ua = require('universal-analytics');
var visit = ua('UA-84701870-2', {https: true});
var http = 'https://';
if ((process.env.NODE_ENV || 'development') === 'development') {
    http = 'http://';
}


module.exports = function(app, redis){
    app.get("/api/bt/client_token", function (req, res) {
        gateway.clientToken.generate({}, function (err, response) {
            res.send(response.clientToken);
        });
    });

    app.post("/api/bt/addcustomer", function (req, res) {
        var teamid = req.body.teamid; 
        var nonce = req.body.payment_method_nonce;
        var subID = (req.body.subid || "38rm");
        console.info("New subscription for " + req.body.teamname + "(" + req.body.subid + ")");

        gateway.customer.find("team_" + teamid, function(err, customer) {
            if (err) {
                if (err.type = "notFoundError") {
                    //Customer Not found, create one
                    gateway.customer.create({
                        id: "team_" + teamid,
                        firstName: req.body.teamname,
                        paymentMethodNonce: nonce,
                        email: req.body.email
                    }, function (err, result) {
                        if (err) { console.error("Error in POST bt/addcustomer."); res.status(500).send({ error: "Error creating customer." }); } else {
                            if (result.customer && req.body.signupuserid) {
                                var method = 0;
                                var payToken = result.customer.paymentMethods[method].token;
                                var userID = req.body.signupuserid;

                                request.post({
                                    url: 'https://' + req.get('host') + '/api/bt/addsub',
                                    body: {
                                        payToken: payToken,
                                        planID: subID
                                    },
                                    json: true
                                }, function(err, results) {
                                    if (results.statusCode == 500) {
                                        res.sendStatus(500);
                                    }
                                    else {
                                        res.sendStatus(200);
                                    }
                                });
                            } else {
                                console.error("Couldn't find payment token or signupuserid");
                                res.status(404).send("Sorry, but we couldn't verify your payment information or your Slack account.  Please try again");
                            }
                        }
                    });
                }
                else {
                    console.error(err);
                    res.status(500).send({ error: "Error creating for your customer record."})
                }
            } else {
                //Customer found, just add the sub
                gateway.paymentMethod.create({
                    customerId: customer.id,
                    paymentMethodNonce: nonce,
                    options: {
                        makeDefault: true
                    }
                }, function (err, result) {
                    var payToken = result.paymentMethod.token;
                    request.post({
                        url: 'https://' + req.get('host') + '/api/bt/addsub',
                        body: {
                            payToken: payToken,
                            planID: subID
                        },
                        json: true
                    }, function(err, results) {
                        if (results.statusCode == 500) {
                            res.sendStatus(500);
                        }
                        else {
                            res.sendStatus(200);
                        }
                    });
                });
            }
        });
    });

    app.post("/api/bt/addsub", function(req, res) {
        var subOBJ = {
            paymentMethodToken: req.body.payToken,
            planId: req.body.planID
        };

        gateway.subscription.create(subOBJ, function (err, result2) {
            if (err) { console.error(err); }
            if (err || result2.success == false) {
                console.log("Problem in creating subscription");
                console.log(result2);
                console.log(err);
                res.sendStatus(500);
            }
            else {
                visit.event("Subscription", "Created", result2.id, 10).send();
                res.sendStatus(200);
            }
        });
    });

    app.post('/api/bt/inbound', function(req, res) {
        gateway.webhookNotification.parse(
            req.body.bt_signature,
            req.body.bt_payload,
            function (err, webhookNotification) {
                if (webhookNotification) {
                    console.log("[Braintree Webhook Received " + webhookNotification.timestamp + "] | Kind: " + webhookNotification.kind);
                }
                else {
                    console.log("[Braintree Webhook Received UNABLE TO PARSE error]");
                    console.log(req.body);
                }
            }
        );
        res.status(200).send();
    });

    app.post('/api/bt/cancel', function(req, res) {
        if (req.body.subid) {
            gateway.subscription.cancel(req.body.subid, function (err, result) {
                if (err || !(result)) { console.error(err); res.sendStatus(500); } else {
                    if (result.success == true) {
                        res.redirect('/sub-canceled');
                    }
                    else {
                        console.error("Error canceling subscription");
                        res.status(404).send("Something went wrong canceling that subscription.")
                    }
                }
            });
        } else {
            console.error("Error looking for subscription to cancel");
            res.status(404).send("That subscription ID either was already canceled or was not correct.")
        }
    });

    app.get('/api/bt/subscriptions', function(req, res) {
        var thesubs = [];
        if (req.header('Authorization') == process.env.clientSecret) {
            var stream = gateway.subscription.search(function (search) {
                search.status().is("Active");
            });

            stream.on("data", function (subscription) {
                thesubs.push(subscription);
            });

            stream.on("end", function () {
                console.log("SUBSCRIPTION COUNT | ", thesubs.length)
                res.json(thesubs);
            });
        } else {
            res.sendStatus(401);
        }
    });

    /**
     * @api {get} /bt/customers Get Customers
     * @apiName BTGetCustomers
     * @apiGroup Braintree Gateway
     *
     * @apiVersion 1.0.3
     * @apiUse StdAuth
     * @apiDescription This end point gets **customers** which are different than **subscriptions**
     */
    app.get('/api/bt/customers', function(req, res) {
        var thecustomers = [];
        if (req.header('Authorization') == process.env.clientSecret) {
            var stream = gateway.customer.search(function (search) {
                //search.status().is("Active");
            });

            stream.on("data", function (cust) {
                thecustomers.push(cust);
            });

            stream.on("end", function () {
                console.log("CUSTOMER COUNT | ", thecustomers.length)
                res.json(thecustomers);
            });
        } else {
            res.sendStatus(401);
        }
    });

    /**
     * @api {get} /bt/customers/simple Get Simple Customers
     * @apiName BTGetCustomersSimple
     * @apiGroup Braintree Gateway
     *
     * @apiVersion 1.0.3
     * @apiUse StdAuth
     * @apiDescription This end point gets **customers** which are different than **subscriptions**,
     * and a small subset of the fields available
     */
    app.get('/api/bt/customers/simple', function(req, res) {
        var thecustomers = [];
        if (req.header('Authorization') == process.env.clientSecret) {
            request.get({
                url: http + req.get('host') + '/api/bt/customers',
                headers: { Authorization: process.env.clientSecret}
            }, function(err, custinfo) {
               if (err) {console.log(err); res.sendStatus(500)} else {
                   var customers = JSON.parse(custinfo.body);
                   let simpleCustomers = [];

                            for (i = 0; i < customers.length; i++) {
                                let thisCust = customers[i];

                                //TODO: Danger, this is getting the "0th" credit card and "0th" subscription
                                simpleCustomers.push({
                                    id: thisCust.id,
                                    firstName: thisCust.firstName,
                                    email: thisCust.email,
                                    subscription: {
                                        status: thisCust.creditCards[0].subscriptions[0].status,
                                        planId: thisCust.creditCards[0].subscriptions[0].planId,
                                        createdAt: thisCust.creditCards[0].subscriptions[0].createdAt,
                                        trialPeriod: thisCust.creditCards[0].subscriptions[0].trialPeriod, 
                                        firstBillingDate: thisCust.creditCards[0].subscriptions[0].firstBillingDate,
                                        nextBillingDate: thisCust.creditCards[0].subscriptions[0].nextBillingDate,
                                        nextBillingPeriodAmount: thisCust.creditCards[0].subscriptions[0].nextBillingPeriodAmount,
                                        daysPastDue: thisCust.creditCards[0].subscriptions[0].daysPastDue,

                                    }
                                })
                            }

                            res.json(simpleCustomers);
               }
            });
        } else {
            res.sendStatus(401);
        }
    });

};

function notimplemented(res) {
    res.status(500).send({ error: "Not yet implemented"});
}