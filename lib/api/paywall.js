/**
 * Created by Brendan on 2/10/19.
 */
var request = require('request');
var moment = require('moment-timezone');
var ua = require('universal-analytics');
var visit = ua('UA-84701870-2', {https: true});
var debug = false;
var http = 'https://';
if ((process.env.NODE_ENV || 'development') === 'development') {
    http = 'http://';
}

module.exports = function(app, redis){
    var databaseClient = redis.createClient(process.env.REDIS_URL);

    /**
     * @api {get} /paywall/update Protect paying customers
     * @apiName UpdatePaying
     * @apiGroup Paywall
     *
     * @apiVersion 1.0.3
     * @apiUse StdAuth
     * @apiDescription This API is designed to ensure that paying customers are "never" on the `paywall:VIOLATING` list
     * 
     * This is designed to give confience in using `paywall:VIOLATING` to send paywall messages
     * Keeping `paywall:VIOLATING` up to date will let us collect stats on non-paying customers
     * and make a more strict paywall as long as paying customers aren't on `paywall:VIOLATING` as a starting point
     * 
     * Order of operations is: 
     * 1. Go to Braintree and get paying customer IDs
     * 2. Delete ALL of `paywall:PAYING`
     * 3. Add all paying customers to `paywall:PAYING`
     * 4. Remove all paying customers from `paywall:VIOLATING`
     */
    app.get('/api/paywall/update', function(req, res) {
        if (req.header('Authorization') == process.env.clientSecret) {
            // First, get customers
            request.get({
                url: http + req.get('host') + '/api/bt/customers/simple',
                headers: { Authorization: process.env.clientSecret}
            }, function(err, custinfo) {
               if (err) {console.log(err); res.sendStatus(500)} else {
                   var customers = JSON.parse(custinfo.body);

                   // Then, delete all `paywall:PAYING`
                   databaseClient.del("paywall:PAYING", function(err2, results) {
                       if (err2) { console.error(err2); res.sendStatus(500); } else {
                            // Then, add all paying customers to PAYING
                            // AND delete from VIOLATING

                            let payingCustomers = [];
                            var multi = databaseClient.multi();

                            for (i = 0; i < customers.length; i++) {
                                if (customers[i].subscription.status == "Active" || customers[i].subscription.status == "Pending") {
                                    payingCustomers.push(customers[i]);
                                    let custId = customers[i].id.replace("team_", "");
                                    let plan = getPlanById(customers[i].subscription.planId)
                                    multi.sadd("paywall:PAYING", custId);
                                    multi.srem("paywall:VIOLATING", custId)
                                    multi.set(`team:${custId}:plan`, plan)
                                }
                            }

                            multi.exec(function(err2, allpaying) {
                                if (err2) { console.error(err2); res.sendStatus(500); } else {
                                    // Then, remove all paying customers from `paywall:VIOLATING`
                                    res.json(allpaying);
                                    //res.json(payingCustomers);
                                }
                            })
                       }
                   })

               }
            });
        }
        else {
            res.sendStatus(401);
        }
    });

    /**
     * @api {get} /paywall/violating Find violators
     * @apiName FindViolators
     * @apiGroup Paywall
     *
     * @apiVersion 1.0.3
     * @apiUse StdAuth
     * @apiDescription This API finds true violators of the terms of service, and adds them to the `paywall:VIOLATING` list
     * 
     * It calls /api/paywall/violating/details and adds anyone returned in `violators` to the violating list.
     * 
     * It also then adds or updates a key team:`team_id`:paywall:months to track for how many (and in which) months a customer was in violation
    */
    app.get('/api/paywall/violating', function(req, res) {
        if (req.header('Authorization') == process.env.clientSecret) {
            request.get({
                url: http + req.get('host') + '/api/paywall/violating/details',
                headers: { Authorization: process.env.clientSecret}
            }, function(err, teaminfo) {
            if (err) {console.log(err); res.sendStatus(500)} else {
                var ret = JSON.parse(teaminfo.body);
                let bad = ret.violators;

                var multi = databaseClient.multi();

                for (i = 0; i < bad.length; i++) {
                    multi.sadd("team:" + bad[i] + ":paywall:months", gettoday('month'));
                    multi.sadd("paywall:VIOLATING", bad[i]);
                }

                multi.exec(function(err2, returner) {
                    if (err2) { console.error(err2); res.sendStatus(500); } else {
                        res.json(returner);
                    }
                })

            }
            });
        }
        else {
            res.sendStatus(401);
        }
    });

    

    /**
     * @api {get} /paywall/violating/details Violator details
     * @apiName ViolatorsDetails
     * @apiGroup Paywall
     *
     * @apiVersion 1.0.3
     * @apiUse StdAuth
     * @apiDescription This API finds true violators of the terms of service, and returns all the details
     * 
     * It combines 2 APIs to calculate the true violators:
     * 
     * * The /api/paywall/tos API to find users who are above the free TOS limits
     * * The /api/teams/paying API to find those user who are actually paying us.
     * 
     * The /api/paywall/violating route usese these details to add violators to the `paywall:VIOLATING` list
    */
    app.get('/api/paywall/violating/details', function(req, res) {
        if (req.header('Authorization') == process.env.clientSecret) {
            request.get({
                url: http + req.get('host') + '/api/paywall/tos',
                headers: { Authorization: process.env.clientSecret}
            }, function(err, teaminfo) {
            if (err) {console.log(err); res.sendStatus(500)} else {
                var tosover = JSON.parse(teaminfo.body);

                request.get({
                    url: http + req.get('host') + '/api/teams/paying',
                    headers: { Authorization: process.env.clientSecret}
                }, function(err, payingteaminfo) {
                if (err) {console.log(err); res.sendStatus(500)} else {
                    var payingteams = JSON.parse(payingteaminfo.body);

                    let TOSf = tosover.map(function (team) {
                        return team.slackID
                    })

                    let violators = TOSf.filter(function (pos) {
                        return !(payingteams.includes(pos))
                    })

                    res.json({
                        tos: TOSf,
                        paying: payingteams,
                        violators: violators
                    });
        
                }
                });

            }
            });
        }
        else {
            res.sendStatus(401);
        }
    });

    /**
     * @api {get} /paywall/tos Check TOS
     * @apiName CheckTOS
     * @apiGroup Paywall
     *
     * @apiVersion 1.0.3
     * @apiUse StdAuth
     * @apiDescription This API looks for customers who are over the free tier TOS limits.
     * 
     * Currently that is only defined as greater than 4 users.
    */
   app.get('/api/paywall/tos', function(req, res) {
        if (req.header('Authorization') == process.env.clientSecret) {
            request.get({
                url: http + req.get('host') + '/api/admin/stats/byteam',
                headers: { Authorization: process.env.clientSecret}
            }, function(err, byteaminfo) {
            if (err) {console.log(err); res.sendStatus(500)} else {
                //Find teams using more than 4 users
                var byteam = JSON.parse(byteaminfo.body);

                let greaterThan4 = byteam.filter(function (team) {
                    return team.usercount > 4;
                })
                res.json(greaterThan4);

            }
            });
        }
        else {
            res.sendStatus(401);
        }
    });

    /**
     * @api {get} /paywall/tos Check TOS
     * @apiName CheckTOS
     * @apiGroup Paywall
     *
     * @apiVersion 1.0.3
     * @apiUse StdAuth
     * @apiDescription This API looks for customers who are over the free tier TOS limits.
     * 
     * Currently that is only defined as greater than 4 users.
    */
   app.get('/api/paywall/tos', function(req, res) {
        if (req.header('Authorization') == process.env.clientSecret) {
            request.get({
                url: http + req.get('host') + '/api/admin/stats/byteam',
                headers: { Authorization: process.env.clientSecret}
            }, function(err, byteaminfo) {
            if (err) {console.log(err); res.sendStatus(500)} else {
                //Find teams using more than 4 users
                var byteam = JSON.parse(byteaminfo.body);

                let greaterThan4 = byteam.filter(function (team) {
                    return team.usercount > 4;
                })
                res.json(greaterThan4);

            }
            });
        }
        else {
            res.sendStatus(401);
        }
    });

    /**
     * @api {get} /teams/nonpaying Nonpaying
     * @apiGroup Paywall
     *
     * @apiVersion 1.0.2
     * @apiSuccess {Array} All nonpaying team IDs
     * @apiDescription Get all nonpaying teams
     * 
     * This API will return all of the teams currently in the `paywall:VIOLATING` key
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     [
     *       "T123ABC",
     *       "T789ZYX"
     *     ]
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *       "error": "Internal Server Error"
     *     }
     */
    app.get('/api/teams/nonpaying', function(req, res) {
        if (debug) { console.log(req.body); }
        if (req.header('Authorization') == process.env.clientSecret) { 
            visit.event("Teams", "List non-paying teams").send();
            var listitems = [];
            databaseClient.smembers("paywall:VIOLATING", function(err, listoflists) {
                if (!err) {
                    res.send(listoflists);
                }
                else {
                    res.status(500).send({ error: err });
                }
            });
        } else {
            res.sendStatus(401);
        }
    });

    app.get('/api/teams/paying', function(req, res) {
        if (debug) { console.log(req.body); }
        if (req.header('Authorization') == process.env.clientSecret) {
            visit.event("Teams", "List paying teams").send();
            var listitems = [];
            databaseClient.smembers("paywall:PAYING", function(err, listoflists) {
                if (!err) {
                    res.send(listoflists);
                }
                else {
                    res.status(500).send({ error: err });
                }
            });
        } else {
            res.sendStatus(401);
        }
    });
}

function gettoday(type) {
    switch (type) {
        case "month":
            return moment.tz("America/New_York").format("YYYY:MM");
        default:
            return moment.tz("America/New_York").format("YYYY:MM:DD");
    }
}

function getPlanById(planId) {
    switch (planId) {
        case "rmgm":
            return "standard"
        case "38rm":
            return "enterprise"
        default:
            return "basic"
    }
}