/**
 * Created by Brendan on 8/18/16.
 */
const uuidv1 = require('uuid/v1');
var request = require('request');
var braintree = require("braintree");
var gateway = require('../braintree.js');

module.exports = function(app, redis){
    var databaseClient = redis.createClient(process.env.REDIS_URL);

    app.get('/api/slacksignin', function(req, res) {
        var sURL = 'https://slack.com/api/oauth.access?client_id=MYCLIENTID&client_secret=MYCLIENTSECRET&code=XXXYYYZZZ&redirect_uri=RDURI';
        var code = req.query.code;
        if (process.env.clientId && process.env.clientSecret && code) {
            sURL = sURL.replace("MYCLIENTID", process.env.clientId);
            sURL = sURL.replace("MYCLIENTSECRET", process.env.clientSecret);
            sURL = sURL.replace("XXXYYYZZZ", code);
            sURL = sURL.replace("RDURI", 'https%3A%2F%2Fwww.listme.chat%2Fapi%2Fslacksignin');

            request(sURL, function(err, results) {
                if (err) { console.error(err); res.status(500).send("Sorry, something went wrong with Slack sign in.  Please try again.")}
                else {
                    var info = JSON.parse(results.body);
                    if (info.ok == true) {
                        var guid = uuidv1();
                        databaseClient.multi()
                            .hset("login:" + guid, "access_token", info.access_token)
                            .hset("login:" + guid, "userid", info.user.id)
                            .hset("login:" + guid, "name", info.user.name)
                            .hset("login:" + guid, "teamid", info.team.id)
                            .expire("login:" + guid, 300)
                            .exec();

                        res.redirect('/account?logintoken=' + guid);
                    } else {
                        console.error("Error with sign in with slack");
                        console.error(results.body);
                        res.status(500).send("Sorry, something went wrong with getting sign in information from Slack.  Please try again.");
                    }
                }
            });
        }
        else {
            res.status(500).send({ error: "Something went wrong with the connection to Slack"})
        }
    });

    app.get('/api/checklogin', function(req, res) {
        var guid = req.query.logintoken;
        if (guid) {
            databaseClient.hgetall("login:" + guid, function(err, results) {
                if (err) { console.error(err); res.status(500).send("Error getting login information.  Please try again.")} else {
                    if (results) {
                        try {
                            var slackinfo = results;
                            var sendIt = { slack: slackinfo };
                            gateway.customer.find("team_" + slackinfo.teamid, function(err, customer) {
                                if (err) { console.error(err); }
                                sendIt.btcust = customer
                                res.json(sendIt);
                            });
                        } catch (error) {
                            console.error(error);
                            res.json(sendIt);
                        }
                        
                    } else { res.status(404).send("Login information not found"); }
                }
            })
        } else {
            res.status(401).send("No login token provided");
        }
    });

};

function notimplemented(res) {
    res.status(500).send({ error: "Not yet implemented"});
}