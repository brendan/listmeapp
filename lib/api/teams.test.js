require('dotenv').config('../../../.env')

const slacklist     = require('../../jest/mock-slacklist')
const Teams         = require('./teams')
const teams         = new Teams(slacklist)

describe("Teams API methods", () => {
    describe("getTeams", () => {
        describe("with a bad authorization header", () => {
            test("Should return 401", () => {
                req = new slacklist.express.req('aaa');
                res = new slacklist.express.res();
                teams.getTeams(req, res)
                expect(res.sendStatus).toHaveBeenCalledWith(401);
            })
        })
        describe("with the correct authorization header", () => {
            test("Should return a 200", () => {
                // TODO: Make this actually work
                expect(1).toBe(1);
            })
        })
    })

    describe("getSingleTeam", () => {
        describe("with a bad authorization header", () => {
            test("Should return 401", () => {
                req = new slacklist.express.req('aaa');
                res = new slacklist.express.res();
                teams.getSingleTeam(req, res)
                expect(res.sendStatus).toHaveBeenCalledWith(401);
            })
        })
    })

    describe("deleteTeam", () => {
        describe("with a bad authorization header", () => {
            test("Should return 401", () => {
                req = new slacklist.express.req('aaa');
                res = new slacklist.express.res();
                teams.deleteTeam(req, res)
                expect(res.sendStatus).toHaveBeenCalledWith(401);
            })
        })
    })
})