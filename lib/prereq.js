/**
 * Created by Brendan on 8/17/16.
 */
var debugTime = false;
var Botkit = require('botkit');
var request = require('request');
var ua = require('universal-analytics');
var visitor = ua('UA-84701870-2', {https: true});
var morgan = require('morgan');
require('ssl-root-cas').inject();

const slack = require('./slack');

if ((process.env.NODE_ENV || 'development') === 'development') {
    console.log("NODE_ENV = ", process.env.NODE_ENV);
    console.log("Dev environment, loading environment varaibles from file");
    console.log("clientSecret", process.env.clientSecret);
    debugTime = true;
}

if (!process.env.clientId || !process.env.clientSecret || !process.env.port) {
    console.log('Error: Specify clientId clientSecret and port in environment');
    //process.exit(1);
}

var redisStorage = require('botkit-storage-redis')({ url: process.env.REDIS_URL});

var controller = Botkit.slackbot({
    storage: redisStorage
}).configureSlackApp(
    {
        clientId: process.env.clientId,
        clientSecret: process.env.clientSecret,
        scopes: ['commands']
    }
);

var oauth = require('./oauth')(controller, process.env.port || "5000");

var slacklists = function Constructor() {
    this.express    = require('express');
    this.hbs        = require('handlebars');
    this.expresshbs = require('express-handlebars');
    this.bodyparser = require('body-parser');
    this.redis      = require("redis");
    this.port       = process.env.port || "5000";
    this.vist       = visitor;
    const Db        = require('./db')
    this.db         = new Db(this);
    this.databaseClient = this.db.databaseClient
    this.logger     = require('./helpers/logger')
 
    this.apiLog =  (msg, caller, req) => {
        this.logger.info(`🤙 API CALL: ${msg}`, 
            { caller: `api.${caller}`, 
            api: true, hostname: req.hostname, path: req.path, 
            details: { type: 'api', file: "api.js"} 
        });
    }
    
    this.tierLog = (fet, caller, allowed, req) => {
        this.logger.info(`💸 tierHandler for ${req.body.team_domain}`, {
            caller: `tierH.${caller}`,
            details: {
                team: req.body.team_domain,
                teamid: req.body.team_id
            },
            feature: fet,
            allowed: allowed
        });
    }


    this.slack      = slack;
    this.send       = slack.send;
    
    this.webApp     = controller.webserver;

    this.webApp.use(this.bodyparser.json() );       // to support JSON-encoded bodies
    this.webApp.use(this.bodyparser.urlencoded({     // to support URL-encoded bodies
        extended: true
    }));

    this.webApp.engine('handlebars', this.expresshbs(require('./www/hbs-config')));
    this.webApp.set('view engine', 'handlebars');

    if (!(debugTime)) { this.webApp.use(morgan("combined")); }
    this.debug  = debugTime;
    
    if (!(process.env.devtime == 'true')) {
        var enforce = require('express-sslify');
        this.webApp.use(enforce.HTTPS({ trustProtoHeader: true }));
    }

    this.request = request;
    this.comparser = require('../lib/commandparser.js');
    this.prefs = require('../lib/prefs');

    var postmark = require("postmark");
    this.emailclient = new postmark.Client("c26c930e-781a-465b-869b-232cc8dbedd5");
};

slacklists.prototype._log = function (log) {
    if (debugTime) {
        console.log(log);
    }
};

slacklists.prototype._error = function(err) {
    if (debugTime) {
        console.error(err);
    }
};

module.exports = slacklists;