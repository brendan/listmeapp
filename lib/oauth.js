/* */
var request = require('request');
var ua = require('universal-analytics');
var visitor = ua('UA-84701870-2', {https: true});

module.exports = function(controller, port){
    controller.setupWebserver(port,function(err,webserver) {
        controller.createWebhookEndpoints(controller.webserver);
        controller.createOauthEndpoints(controller.webserver,function(err,req,res) {
            if (err) {
                if (req.query.error == "access_denied") {
                    res.render("error", {
                        theerror: "Sorry, but we need to have access to your Slack account in order to use listMe.  Please return to the home page to start over."
                    })
                } else {
                    console.error(err);
                    res.render("error", {
                        theerror: err
                    });
                }
            } else {
                var postbody = {
                    team_id: req.identity.team_id,
                    team_domain: (req.identity.team || req.identity.team_domain),
                    identity: req.identity
                };

                var opts = {
                    url: 'https://' + req.get('host') + '/api/teams',
                    method: "POST",
                    headers: {
                        Authorization: process.env.clientSecret
                    },
                    json: true,
                    body: postbody
                };
                request(opts, function(err, theresponse) {
                    if (err) {
                        console.error(err);
                        console.error("Error with request POSTing to /api/teams");
                        res.status(500).send("Something went wrong, please try again.");
                    } else {
                        var opts2 = {
                            url: 'https://' + req.get('host') + '/api/bt/client_token',
                            method: "GET",
                            headers: {
                                Authorization: process.env.clientSecret
                            }
                        };
                        request(opts2, function(err2, secondresp) {
                            if (err2) { res.status(500).send("Something went wrong, please try again."); console.error("Error getting braintree client token."); }
                            var theToken = { token: secondresp.body };
                            visitor.pageview('/signup').send();
                            visitor.event("Subscription", "Sign-up complete").send();
                            res.render('signup', { team: req.identity, clientT: theToken });
                        });
                    }
                });
            }
        });
    });

};