/**
 * @apiDefine StdAuth
 * @apiHeader {String} Authorization Shared secret for the app to identify itself to the API.
 * 
 *     {"Authorization": "abc123xyz789"}
*/



//  LEGACY DOCS

/**
     * @api {get} /teams/:team_id/details Get team details
     * @apiName GetTeamDetails
     * @apiGroup Teams
     *
     * @apiVersion 1.1.0
     * @apiUse StdAuth
     * @apiParam {String} team_id     The ID of the team to get (e.g. `T123ABC98`).
     * 
     * @apiDescription Get all the keys and various other details about a given team.
     * 
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
     *   details: {
     *       "paid": "false",
     *       "slackID": "TA08RSMHD",
     *       "slackTeamName": "citycatvetsgroup",
     *       "email": "unk"
     *      },
     *   users: [
     *       "ABC321X",
     *       "ZYZ987Y"
     *      ]
     *   paying: false,
     *   paywallMonths: [
     *       "2019:02"
     *      ]
     *   allkeys: [
     *       ...
     *      ]
     *  }
     */
    /* OLD DEFINITION
    app.get('/api/teams/:team_id/details', function(req, res) {
        if (debug) { console.log(req.body); }
        if (req.header('Authorization') == process.env.clientSecret) {
            var team = 'team:' + req.params.team_id;

            var multi = databaseClient.multi();

            let statmonth = "stats:MonthlyActiveUsersByTeam:" + team

            multi.keys(team + "*");
            multi.smembers(team + ":paywall:months");
            multi.sismember("paywall:PAYING", req.params.team_id);
            multi.hgetall(team + ":details");
            multi.smembers(team + ":users");
            multi.keys(statmonth + "*");
            multi.get(team + ":plan");

            multi.exec(function(err, results) {
                if (err) { console.error(err); res.sendStatus(500); } else {
                    mau = results[5].map(function (month) {
                        return month.replace(statmonth + ":", "");
                    })
                    mau.sort().reverse();

                    res.json(
                        {
                            details: results[3],
                            users: results[4],
                            paying: results[2] == 1,
                            plan: results[6],
                            paywallMonths: results[1],
                            activeMonths: mau,
                            statKeys: results[5],
                            allkeys: results[0]
                        }
                    );
                }
            });
        }
        else {
            res.sendStatus(401);
        }
    });
    */