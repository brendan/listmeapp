/**
 * Created by Brendan on 8/18/16.
 */

var request     = require('request');
var debug       = false;
const dt        = require('./helpers/datetime')

module.exports = function(slacklist, redis){
    var databaseClient  = redis.createClient(process.env.REDIS_URL);
    var app = slacklist.webApp;

    const Teams     = require('./api/teams')
    const teams     = new Teams(slacklist)

    const Feedback  = require('./api/feedback')
    const feedback  = new Feedback(slacklist)

    const Users     = require('./api/users')
    const users     = new Users(slacklist)

    const Lists     = require('./api/lists')
    const lists     = new Lists(slacklist)

    if (!app.listMeTestMode) {
        var bt = require('./api/braintree')(app, redis);
        var slack = require('./api/slack')(app, redis);
        var adminapi = require('./api/admin')(app, redis);
        var paywall = require('./api/paywall')(app, redis);
        var betaapi = require('./api/beta')(app, redis);
    }

    app.all('/api*', function(req, res, next) {
        slacklist.apiLog("CATCH ALL", "general", req)
        next();
    })

    /**
     * @api {delete} /teams/:team_id Delete team
     * @apiName api.teams.delete
     * @apiGroup Teams
     *
     * @apiVersion 1.0.0
     * @apiUse StdAuth
     * @apiParam {String} team_id     The ID of the team to get (e.g. `T123ABC98`).
     * 
     * @apiDescription `Not yet implmeneted`
     */
    app.delete('/api/teams/:team_id', teams.deleteTeam);

    /**
     * @api {get} /teams Get all teams
     * @apiName api.teams.getAll
     * @apiGroup Teams
     *
     * @apiVersion 1.0.0
     * @apiUse StdAuth
     * 
     * @apiDescription Returns an array of all team IDs
     * 
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     [
     *       "T0987ZBCDE",
     *       "T2468QWERT",
     *       ...
     *       "T1234ASDF"
     *     ]
     */
    app.get('/api/teams', teams.getTeams);

    /**
     * @api {get} /teams/:team_id Get team
     * @apiName api.teams.get
     * @apiGroup Teams
     *
     * @apiVersion 1.0.2
     * @apiUse StdAuth
     * 
     * @apiParam {String} team_id     The ID of the team to get (e.g. `T123ABC98`).
     */
    app.get('/api/teams/:team_id', teams.getSingleTeam);

    /**
     * @api {get} /teams/:team_id/details Get team details
     * @apiName api.teams.get.details
     * @apiGroup Teams
     *
     * @apiVersion 2.0.0
     * @apiUse StdAuth
     * @apiParam {String} team_id     The ID of the team to get (e.g. `T123ABC98`).
     * 
     * @apiDescription Get all the keys and various other details about a given team.
     * 
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
     *   details: {
     *       "paid": "false",
     *       "slackID": "TA08RSMHD",
     *       "slackTeamName": "citycatvetsgroup",
     *       "email": "unk"
     *      },
     *   users: [
     *       "ABC321X",
     *       "ZYZ987Y"
     *      ]
     *   paying: false,
     *   paywallMonths: [
     *       "2019:02"
     *      ]
     *   allkeys: [
     *       ...
     *      ]
     *  }
     */
    app.get('/api/teams/:team_id/details', teams.getTeamDetails);

    /**
     * @api {get} /teams/:team_id/plan Get team plan
     * @apiName api.teams.get.plan
     * @apiGroup Teams
     *
     * @apiVersion 2.0.0
     * @apiUse StdAuth
     * @apiParam {String} team_id     The ID of the team to get (e.g. `T123ABC98`).
     * 
     * @apiDescription Get the plan for a given team.
     * 
     * Possible values: `basic`, `standard`, `enterprise`
     * 
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
     *   plan: "standard"
     * }
     */
    app.get('/api/teams/:team_id/plan', teams.getTeamPlan);

    /**
     * @api {post} /teams Post new team
     * @apiName api.teams.post
     * @apiGroup Teams
     *
     * @apiVersion 1.0.0
     * @apiUse StdAuth
     * 
     * @apiDescription Adds a new team to the various keys needed.
     */
    app.post('/api/teams', teams.addTeam);

    /**
     * @api {post} /feeedback Post new feedback
     * @apiName api.feedback.post
     * @apiGroup Feedback
     *
     * @apiVersion 1.0.0
     * @apiUse StdAuth
     * 
     * @apiDescription Add new feedback.
     */
    app.post('/api/feedback', feedback.sendFeedback);

    /**
     * @api {post} /teams/:team_id/users Post new user
     * @apiName api.teams.users.post
     * @apiGroup Teams
     *
     * @apiVersion 1.0.0
     * @apiUse StdAuth
     * 
     * @apiDescription Add new user to a team.
     */
    app.post('/api/teams/:team_id/users', users.addUser);

    /**
     * @api {get} /teams/:team_id/users Get a team's users
     * @apiName api.teams.users.get
     * @apiGroup Teams
     *
     * @apiVersion 1.0.0
     * @apiUse StdAuth
     * 
     * @apiParam {String} team_id     The ID of the team to get (e.g. `T123ABC98`).
     * 
     * @apiDescription Get all of a given team's user.
     */
    app.get('/api/teams/:team_id/users', users.getUser);
    
    /**
     * @api {get} /lists Get all lists
     * @apiName api.lists.getAll
     * @apiGroup Lists
     *
     * @apiVersion 1.0.0
     * @apiUse StdAuth
     * 
     * @apiDescription Get all of the lists of lists.
     */
    app.get('/api/lists', lists.getAll);

    /**
     * @api {get} /team/:team_id/lists/:list_id Get a list
     * @apiName api.lists.get
     * @apiGroup Lists
     *
     * @apiVersion 1.0.0
     * @apiUse StdAuth
     * 
     * @apiParam {String} team_id     The ID of the team to get (e.g. `T123ABC98`).
     * @apiParam {String} list_id     The ID of the list to get (e.g. `CABC123XX`).
     * 
     * @apiDescription Get all of the items in a particular list.
     */
    app.get('/api/team/:team_id/lists/:list_id', function(req, res){
        if (debug) { console.log(req.body); }
        slacklist.apiLog("GET A LIST", "lists.get", req)
        if (req.header('Authorization') == process.env.clientSecret) { 
            var listID = getListID(req.params.team_id, req.params.list_id);
            if (listID) {
                var sentToken = req.body.token;
                verifyPost(sentToken, req, res, databaseClient, getList);
            }
            else {
                res.status(400).send({ error: "No channel or list specified."});
            }
        } else { res.sendStatus(401); }
    });

    /**
     * @api {post} /team/:team_id/lists/:list_id Add an item to a list
     * @apiName api.lists.items.post
     * @apiGroup Lists
     *
     * @apiVersion 1.0.0
     * @apiUse StdAuth
     * 
     * @apiParam {String} team_id     The ID of the team to get (e.g. `T123ABC98`).
     * @apiParam {String} list_id     The ID of the list to get (e.g. `CABC123XX`).
     * 
     * @apiDescription Add an item to a list.
     */
    app.post('/api/team/:team_id/lists/:list_id', function(req, res) {
        if (debug) { console.log(req.body); }
        slacklist.apiLog("ADD ITEM TO A LIST", "lists.post", req)
        var sentToken = req.body.token;
        //this event is tracked in fn addItemToList
        verifyPost(sentToken, req, res, databaseClient, addItemToList);
    });
    
    /**
     * @api {get} /team/:team_id/lists/:list_id/item/:item_num Add an item to a list
     * @apiName api.lists.items.get
     * @apiGroup Lists
     *
     * @apiVersion 1.0.0
     * @apiUse StdAuth
     * 
     * @apiParam {String} team_id     The ID of the team to get (e.g. `T123ABC98`).
     * @apiParam {String} list_id     The ID of the list to get (e.g. `CABC123XX`).
     * @apiParam {Number} item_num    The number of the item on the list to get (e.g. `3`).
     * 
     * @apiDescription Add an item to a list.
     */
    app.get('/api/team/:team_id/lists/:list_id/item/:item_num', function (req, res) {
        if (debug) { console.log(req.body); }
        slacklist.apiLog("GET AN ITEM ON A LIST", "lists.items.get", req)
        var list_id = getListID(req.params.team_id, req.params.list_id);
        var item_num = req.params.item_num;
        getListItem(list_id, item_num, databaseClient, res, sendItem);
    });

    /**
     * @api {delete} /team/:team_id/lists/:list_id/item/:item_num Delete an item from a list
     * @apiName api.lists.items.delete
     * @apiGroup Lists
     *
     * @apiVersion 1.0.0
     * @apiUse StdAuth
     * 
     * @apiParam {String} team_id     The ID of the team to get (e.g. `T123ABC98`).
     * @apiParam {String} list_id     The ID of the list to get (e.g. `CABC123XX`).
     * @apiParam {Number} item_num    The number of the item on the list to get (e.g. `3`).
     * 
     * @apiDescription Delete an item from a list.
     */
    app.delete('/api/team/:team_id/lists/:list_id/item/:item_num', function(req, res) {
        if (debug) { console.log(req.body); }
        slacklist.apiLog("DELETE AN ITEM FROM A LIST", "lists.items.delete", req)
        var list_id = getListID(req.params.team_id, req.params.list_id);
        var item_num = req.params.item_num;
        //this event is tracked in fn deleteItem
        getListItem(list_id, item_num, databaseClient, res, deleteItem);
    });

    /**
     * @api {post} /team/:team_id/lists/:list_id/item/:item_num Change an item on a list
     * @apiName api.lists.items.post
     * @apiGroup Lists
     *
     * @apiVersion 1.0.0
     * @apiUse StdAuth
     * 
     * @apiParam {String} team_id     The ID of the team to get (e.g. `T123ABC98`).
     * @apiParam {String} list_id     The ID of the list to get (e.g. `CABC123XX`).
     * @apiParam {Number} item_num    The number of the item on the list to get (e.g. `3`).
     * 
     * @apiDescription Modify an item on a list.
     */
    app.post('/api/team/:team_id/lists/:list_id/item/:item_num', function(req, res) {
        if (debug) { console.log(req.body); }
        slacklist.apiLog("MODIFY AN ITEM ON A LIST", "lists.items.post", req)
        var list_id = getListID(req.params.team_id, req.params.list_id);
        var item_num = req.params.item_num;
        //this event is tracked in fn deleteItem
        getListItem(list_id, item_num, databaseClient, res, strikeItem);
    });

    /**
     * @api {delete} /team/:team_id/lists/:list_id Delete a list
     * @apiName api.lists.delete
     * @apiGroup Lists
     *
     * @apiVersion 1.0.0
     * @apiUse StdAuth
     * 
     * @apiParam {String} team_id     The ID of the team to get (e.g. `T123ABC98`).
     * @apiParam {String} list_id     The ID of the list to get (e.g. `CABC123XX`).
     * 
     * @apiDescription Delete a list.
     */
    app.delete('/api/team/:team_id/lists/:list_id', function(req, res) {
        if (debug) { console.log(req.body); }
        slacklist.apiLog("DELETE A LIST", "lists.delete", req)
        var list_id = getListID(req.params.team_id, req.params.list_id);
        databaseClient.del(list_id, function(err, result) {
            if (err) { console.log(err); res.sendStatus(500); } else {
                res.sendStatus(404);
            }
        })
    })

};

function addItemToList(req, res, databaseClient) {
    var listID = getListID(req.params.team_id, req.params.list_id);
    databaseClient.sadd("listoflists", listID);

    if (req.body.name) {
        databaseClient.incr(listID + ":nextID", function(error, result) {
            if (error) { console.error(error); res.status(500).send({ error: "Error with listID"} )} else {
                var newTodo = {};
                newTodo.list_id = listID;
                newTodo.name = req.body.name;
                newTodo.id = result.toString();

                databaseClient.multi()
                    .hset(newTodo.list_id, newTodo.id, newTodo.name)
                    .ZINCRBY("stats:items:alltimescore", 1, newTodo.name)
                    .LPUSH("stats:items:runninglist", newTodo.name)
                    .exec();

                res.send(newTodo);
            }
        });
    }
    else {
        res.send({ message: "Created list " + req.params.list_id});
    }
}

function getListID(team_id, list_id) {
    return 'team:' + team_id + ':list:' + list_id;
}

function getList(req, res, databaseClient) {
    var retList = [];
    var list_id = getListID(req.params.team_id, req.params.list_id);
    databaseClient.sismember("listoflists", list_id, function(err, reply) {
        if (err) { console.error(err); }
        if (reply) {
            var listitems = [];
            databaseClient.hgetall(list_id, function(err, items) {
                for(var b in items) {
                    var newitem = {
                        id: b,
                        text: items[b]
                    };
                    listitems.push(newitem);
                }
                retList = listitems;
                sendList(retList, list_id, res, req.body.response_url);
            });
        }
        else {
            sendList(retList, list_id, res, req.body.response_url);
        }
    });
}

function sendList(theList, list_id, res, theURL) {
    if (theList && theList.length > 0) {
        res.send({ list_id: list_id, items: theList, response_URL: theURL});
    }
    else {
        res.status(404).send("List " + list_id + " not found.");
    }
}

function getListItem(list_id, theitemnum, databaseClient, res, cb) {

    if (theitemnum > 0) { theitemnum = theitemnum - 1; }
        var item = {};
        databaseClient.sismember("listoflists", list_id, function(err, reply) {
            if (err) { console.error(err); }
            if (reply) {
                var listitems = [];
                databaseClient.hgetall(list_id, function(err, items) {
                    for(var b in items) {
                        var newitem = {
                            id: b,
                            text: items[b]
                        };
                        listitems.push(newitem);
                    }
                    var item = listitems[theitemnum];
                    cb(item, list_id, theitemnum, res, databaseClient);
                });
            }
            else {
                console.log('getListItem list not found');
                cb(item, list_id, theitemnum, res, databaseClient);
            }
        });
}

function sendItem(theItem, list_id, item_num, res) {
    if (theItem) {
        res.send({ list_id: list_id, items: theItem});
    }
    else {
        res.status(404).send("Item number " + item_num + " in list " + list_id + " not found.");
    }
}

function deleteItem(theItem, list_id, item_num, res, databaseClient) {
    if (theItem) {
        databaseClient.hexists(list_id, theItem.id, function(err, reply) {
            if (reply == 1) {
                databaseClient.hdel(list_id, theItem.id, function (err, reply) {
                    res.sendStatus(204);
                });
            }
            else {
                res.status(404).send({ error: "List item not found to delete."})
            }
        });
    }
    else {
        res.status(404).send({ error: "Cannot find item on that list."})
    }
}

function strikeItem(theItem, list_id, item_num, res, databaseClient) {
    if (theItem) {
        databaseClient.hexists(list_id, theItem.id, function(err, reply) {
            if (reply == 1) {
                databaseClient.hget(list_id, theItem.id, (err, reply) => {
                    if (err) { res.status(404).send({ error: "Error getting list item in STRIKE."}) } else {
                        databaseClient.hmset(list_id, theItem.id, `~${reply}~`, (err, reply) => {
                            if (err) {  res.status(404).send({ error: "Error setting list item in STRIKE."}) } else {
                                res.sendStatus(204);
                            }
                        });
                    }
                });
            }
            else {
                res.status(404).send({ error: "List item not found to strike out."})
            }
        });
    }
    else {
        res.status(404).send({ error: "Cannot find item on that list."})
    }
}

function verifyPost(sendToken, req, res, databaseClient, cb) {
    var team = 'team:' + req.params.team_id;

    cb(req, res, databaseClient);

    var today = dt.gettoday();
    databaseClient.multi()
        .incr("stats:messages:" + today)
        .sadd("stats:DailyActiveTeams:" + today, req.params.team_id)
        .exec();

}
