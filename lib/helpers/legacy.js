function get404img() {
    var imgs = [
        'https://media.giphy.com/media/10YgtvMxLO3Mwo/giphy.gif',
        'https://media.giphy.com/media/l41m1UCZiRDhAuoda/giphy.gif',
        'https://media.giphy.com/media/SCYEgsujYeRfq/giphy.gif',
        'https://media.giphy.com/media/3oEdv9duTLhWoNhcGs/giphy.gif',
        'https://media.giphy.com/media/9J7tdYltWyXIY/giphy.gif',
        'https://media.giphy.com/media/5QlwTKQ3kq3N6/giphy.gif',
        'https://media.giphy.com/media/l41YkFIiBxQdRlMnC/giphy.gif',
        'https://media.giphy.com/media/STaM7W6ySSK9G/giphy.gif',
        'https://media.giphy.com/media/6Q3M4BIK0lX44/giphy.gif',
        'https://media.giphy.com/media/3o6MbqEseuRL7dJ7FK/giphy.gif',
        'https://media.giphy.com/media/3o6MbmDCd6V0FMHDm8/giphy.gif',
        'https://media.giphy.com/media/xT5LMCdS00VYNOleNi/giphy.gif',
        'https://media.giphy.com/media/3orieVdMlaDXF4KuHK/giphy.gif',
        'https://media.giphy.com/media/QaA7ZWX1XE0AU/giphy.gif',
        'https://media.giphy.com/media/TUTAGYL4Jth2o/giphy.gif',
        'https://media.giphy.com/media/3KWHzadf2Iq64/giphy.gif',
        'https://media.giphy.com/media/pKEufUXBqsLi8/giphy.gif'
    ];

    return imgs[Math.floor(Math.random()*imgs.length)];
}