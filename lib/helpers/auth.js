module.exports = {
    intraapp: (req) => {
        return req.header('Authorization') == process.env.clientSecret
    }
}