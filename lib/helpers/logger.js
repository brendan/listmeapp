const { createLogger, format, transports } = require('winston');

const consoleTransport = {

}

const logger = createLogger({
  level: 'info',
  exitOnError: false,
  format: format.json(),
  transports: [
    //new transports.File({ filename: `${appRoot}/logs/<FILE_NAME>.log` }),
    new transports.Console(consoleTransport),
  ],
});

module.exports = logger;