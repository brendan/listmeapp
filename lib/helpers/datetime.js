const moment      = require('moment-timezone');

module.exports = {
    gettoday: (type) => {
        switch (type) {
            case "month":
                return moment.tz("America/New_York").format("YYYY:MM");
            default:
                return moment.tz("America/New_York").format("YYYY:MM:DD");
        }
    }
}