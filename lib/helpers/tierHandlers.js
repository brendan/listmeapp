var request     = require('request');
var Prefs       = require('../prefs');
var prefs       = new Prefs();

function _tierId(tierName) {
    switch(tierName) {
        case "basic":
            return 0
        case "standard":
            return 1
        case "enterprise":
            return 2
        default:
            return 0
    }
}

_featureTier = {
    listReorder: _tierId("standard"),
    editItem: _tierId("standard"),
    strikeItem: _tierId("standard"),
    webApp: _tierId("enterprise"),
    autoUpdateCard: _tierId("enterprise"),
    rbac: _tierId("enterprise"),
    multiList: _tierId("enterprise")
}

function _hasFeature(custTier, featureName) {
    let cTier = _tierId(custTier);
    let fTier = _featureTier[featureName];
    return (cTier >= fTier);
}


var tierHandler = function Constructor() {
    this.check = function(req, num, talk, featureName, cb) {
        var options = {
            url:  prefs._makeURL(null, '/api/teams/' + req.body.team_id + '/plan'),
            json: true,
            headers: {
                "Authorization": process.env.clientSecret
            }
        };
    
        request.get(options, function(err, response, body) {
            if (err) {
                console.error(err);
                return cb(req, num, talk, false);
            }
            else {
                let allowed = _hasFeature(body.plan, featureName);
                return cb(req, num, talk, allowed);
            }
        });
    }
}

module.exports = new tierHandler();