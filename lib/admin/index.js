/**
 * Created by Brendan on 9/23/16.
 */
var passport = require('passport');
var Strategy = require('passport-local').Strategy;
var users = require('./admin-users');


module.exports = function(slacklistApp, redis){
    var app = slacklistApp.webApp;
    var databaseClient = redis.createClient(process.env.REDIS_URL);
    setupPassport(app, databaseClient);
    var prefs = new slacklistApp.prefs();

    app.get('/admin', function(req, res) {
        res.render('adminlogin');
    });

    app.post('/admin',
        passport.authenticate('local', {failureRedirect: '/admin'}),
        function(req, res) {
            res.redirect('/admin-home');
        }
    );

    app.post('/admin-trans', function(req, res) {
        if (req.body.text) {
            slacklistApp.request.get(
                'https://translate.googleapis.com/translate_a/single?client=gtx&sl=auto&dl=en&dt=t&q=' + req.body.text,
                function(err, results) {
                    if (err) { console.error(err); res.status(500).send(err); } else {
                        res.json(results.body);
                    }
                }
            );
        } else {
            res.status(400).send("No text in POST");
        }
    });

    app.get('/admin-logout', function(req, res) {
        req.logout();
        res.redirect('/admin');
    });

    app.get('/admin-beta', require('connect-ensure-login').ensureLoggedIn('/admin'), function(req, res) {
        var options = {
            url: prefs._makeURL(req, '/api/admin/betainfo'),
            method: "GET",
            headers: {
                Authorization: req.user.authHeader
            }
        };
        slacklistApp.request.get(options, function(err, results) {
            if (err) { console.error(err); res.status(500).send("Something bad happened")} else {
                var betas = JSON.parse(results.body);
                res.render('admin-beta', {
                    layout: 'admin.handlebars',
                    navitems: getMenu("Beta Program"),
                    betainfo: betas
                });
            }
        });
    });

    app.get('/admin-dau', require('connect-ensure-login').ensureLoggedIn('/admin'), function(req, res) {
        var options = {
            url: prefs._makeURL(req, '/api/admin/dau'),
            method: "GET",
            headers: {
                Authorization: req.user.authHeader
            }
        };

        slacklistApp.request.get(options, function(err, results) {
            if (err) { console.error(err); res.status(500).send("Something bad happened")} else {
                var theR = JSON.parse(results.body);
                res.render('admin-dau', {
                    layout: 'admin.handlebars',
                    navitems: getMenu("Daily Active Stats"),
                    dauinfo: theR
                });
            }
        });
    });

    app.get('/admin-mau', require('connect-ensure-login').ensureLoggedIn('/admin'), function(req, res) {
        var options = {
            url: prefs._makeURL(req, '/api/admin/stats/bymonth'),
            method: "GET",
            headers: {
                Authorization: req.user.authHeader
            }
        };

        slacklistApp.request.get(options, function(err, results) {
            if (err) { console.error(err); res.status(500).send("Something bad happened")} else {
                var theR = JSON.parse(results.body);
                res.render('admin-mau', {
                    layout: 'admin.handlebars',
                    navitems: getMenu("Monthly Active Stats"),
                    mauinfo: theR
                });
            }
        });
    });

    app.get('/admin-home', require('connect-ensure-login').ensureLoggedIn('/admin'), function(req, res) {
        var options = {
            url: prefs._makeURL(req, '/api/admin/teaminfo'),
            method: "GET",
            headers: {
                Authorization: req.user.authHeader
            }
        };
        slacklistApp.request.get(options, function(err, results) {
            if (err) { console.error(err); res.status(500).send("Something bad happened")} else {
                if (results.statusCode == 401) {
                    res.status(401).send("Ah ah ah, you didn't say the magic word")
                } else {
                    teams = JSON.parse(results.body);
                    options.url = prefs._makeURL(req, '/api/admin/stats');
                    slacklistApp.request.get(options, function(err2, stats) {
                    if (err2)  { res.status(500).send("Error getting stats")} else {
                        res.render('adminhome', {
                            layout: 'admin.handlebars',
                            user: req.user,
                            teams: teams,
                            stats: JSON.parse(stats.body),
                            navitems: getMenu("Dashboard")
                        });
                    }
                    });
                }
            }
        });
    });

    app.get('/admin-paywall', require('connect-ensure-login').ensureLoggedIn('/admin'), function(req, res) {
        var options = {
            url: prefs._makeURL(req, '/api/paywall/violating/details'),
            method: "GET",
            headers: {
                Authorization: req.user.authHeader
            }
        };
        slacklistApp.request.get(options, function(err, results) {
            if (err) { console.error(err); res.status(500).send("Something bad happened")} else {
                if (results.statusCode == 401) {
                    res.status(401).send("Ah ah ah, you didn't say the magic word")
                } else {
                    teams = JSON.parse(results.body);

                    res.render('adminpaywall', {
                        layout: 'admin.handlebars',
                        user: req.user,
                        teams: teams,
                        navitems: getMenu("Paywall")
                    });
                }
            }
        });
    });

    app.get('/admin/team/:team_id', require('connect-ensure-login').ensureLoggedIn('/admin'), function(req, res) {
        var options = {
            url: prefs._makeURL(req, '/api/teams/' + req.params.team_id + '/details'),
            method: "GET",
            headers: {
                Authorization: req.user.authHeader
            }
        };
        slacklistApp.request.get(options, function(err, results) {
            if (err) { console.error(err); res.status(500).send("Something bad happened")} else {
                if (results.statusCode == 401) {
                    res.status(401).send("Ah ah ah, you didn't say the magic word")
                } else {
                    details = JSON.parse(results.body);

                    res.render('adminteamdetails', {
                        layout: 'admin.handlebars',
                        user: req.user,
                        details: details,
                        navitems: getMenu("foo")
                    });
                }
            }
        });
    });

    app.get('/admin-teams', require('connect-ensure-login').ensureLoggedIn('/admin'), function(req, res) {
        var options = {
            url: prefs._makeURL(req, '/api/admin/teaminfo'),
            method: "GET",
            headers: {
                Authorization: req.user.authHeader
            }
        };
        slacklistApp.request.get(options, function(err, results) {
            if (err) { console.error(err); res.status(500).send("Something bad happened")} else {
                teams = JSON.parse(results.body);
                options.url = prefs._makeURL(req, '/api/admin/stats/byteam');
                if (req.query.thismon) {
                    options.url = options.url + "?thismon=" + req.query.thismon;
                }
                slacklistApp.request.get(options, function(err2, stats) {
                    if (err2)  { res.status(500).send("Error getting stats")} else {
                        res.render('adminteams', {
                            layout: 'admin.handlebars',
                            user: req.user,
                            teams: teams,
                            stats: JSON.parse(stats.body),
                            navitems: getMenu("Teams")
                        });
                    }
                });
            }
        });
    });

    app.get('/admin-subdetails', function(req, res) {
        
    });

    app.get('/admin/subs', require('connect-ensure-login').ensureLoggedIn('/admin'), function(req, res) {
        var options = {
            url: prefs._makeURL(req, '/api/bt/subscriptions'),
            method: "GET",
            headers: {
                Authorization: req.user.authHeader
            }
        };

        slacklistApp.request.get(options, function(err, results) {
           if (err) { console.error(err); res.status(500).send("Something went wrong getting subscriptions")} else {
               res.json(results);
           }
        });
    });

    app.get('/admin/tickets', require('connect-ensure-login').ensureLoggedIn('/admin'), function(req, res) {
        var options = {
            url: prefs._makeURL(req, '/api/admin/opentickets'),
            method: "GET",
            headers: {
                Authorization: req.user.authHeader
            }
        };

        slacklistApp.request.get(options, function(err, results) {
           if (err) { console.error(err); res.status(500).send("Something went wrong getting subscriptions")} else {
               res.json(results);
           }
        });
    });
};


function setupPassport(app, databaseClient) {
    passport.use(new Strategy(
        function(username, password, cb) {
            users.findByUsername(username, function(err, user) {
                if (err) {return cb(err); }
                if (!user) { return cb(null, false); }
                if (user.password != password) { return cb(null, false); }
                return cb(null, user);
            });
        }
    ));

    passport.serializeUser(function(user, cb) {
        cb(null, user.id);
    });

    passport.deserializeUser(function(id, cb) {
        users.findById(id, function(err, user) {
            if (err) { return cb(err); }
            cb(null, user);
        });
    });

    var session = require('express-session');
    var RedisStore = require('connect-redis')(session);

    app.use(session({
        store: new RedisStore({
            client: databaseClient
        }),
        secret: 'listMe keyboard cat',
        resave: false,
        saveUninitialized: false
    }));

    //app.use(require('express-session')({ secret: 'listMe keyboard cat', resave: false, saveUninitialized: false}));
    app.use(passport.initialize());
    app.use(passport.session());
}

function getMenu(activeItem) {
    var menu = menuitems;
    for (i = 0; i < menu.length; i++) {
        if (menu[i].title == activeItem) {
            menu[i].active = "active";
        }
    }
    return menu;
}

var menuitems = [
    {
        href: '/admin-home',
        title: 'Dashboard',
        icon: 'dashboard'
    },
    {
        href: '/admin-beta',
        title: 'Beta Program',
        icon: 'share-square-o'
    },
    {
        href: '/admin-dau',
        title: 'Daily Active Stats',
        icon: 'calendar-check-o'
    },
    {
        href: '/admin-teams',
        title: 'Team Stats',
        icon: 'users'
    },
    {
        href: '/admin-mau',
        title: 'Monthly Active Stats',
        icon: 'calendar'
    },
    {
        href: '/admin-paywall',
        title: 'Paywall',
        icon: 'ban'
    }
];
