/**
 * Created by Brendan on 9/23/16.
 */
var records = [
    { id: 1, username: 'listMeadmin', password: 'listMe$admin', displayName: 'List Me Admin', authHeader: 'a440efe36e9f8a64105ddcc164ba5779'}
];

exports.findById = function(id, cb) {
    process.nextTick(function() {
       var idx = id-1;
        if (records[idx]) {
            cb(null, records[idx])
        } else {
            cb(new Error('User ' + id + ' does not exisit.'));
        }
    });
};

exports.findByUsername = function(username, cb) {
    process.nextTick(function() {
        for (var i = 0, len = records.length; i < len; i++) {
            var record = records[i];
            if (record.username === username) {
                return cb(null, record);
            }
        }

        return cb(null, null)
    })
};