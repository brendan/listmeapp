var Logger = require('logdna');
var options = {
    hostname: myHostname,
    ip: ipAddress,
    mac: macAddress,
    app: appName,
    env: envName
};
var apikey = process.env.LOGDNA;

// Defaults to false, when true ensures meta object will be searchable
options.index_meta = true;

// Add tags in array or comma-separated string format:
options.tags = ['logging', 'nodejs', 'logdna'];
// or:
options.tags = 'logging,nodejs,logdna';

// Define a singleton instance
var logger = Logger.setupDefaultLogger(apikey, options);

// Create multiple loggers with different options
var logger = Logger.createLogger(apikey, options);