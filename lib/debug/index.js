const slack         = require('../slack')
const tierHandler   = require('../helpers/tierHandlers');

module.exports = {
    processCommand: (req, item) => {
        var rURL = req.body.response_url;
        var cmdArr = item.split(' ')
        cmd = cmdArr[1]

        switch (cmd) {
            case "feature":
                tierHandler.check(req.body.team_id, cmdArr[2], rURL, featureRet)
                break;
        
            default:
                break;
        }

        slack.send(rURL, `Debug command recieved: ${item}`)
    }
}

function featureRet(rURL, hasFeature) {
    let ret = "DOES NOT"
    if (hasFeature) { ret = "DOES" }
    slack.send(rURL, `Team *${ret}* have feature`)
}