/**
 * Created by Brendan on 8/22/16.
 */
var test1 = 'add do that thing that I have been meaning to do';
var test2 = 'delete 1';
var test3 = 'done 4';
var test4 = 'clear';
var test5 = 'safds 7';

//var testMessage = 'add do that thing that I have been meaning to do'; //put slack message here

//var testMessageTextObject = new CommandObject(testMessage); //initialize message object

//console.log(testMessageTextObject); //return results

var parse = function Constructor(slackMessage) {
    this.wholeMessage = slackMessage;
    this.parseCommand = slackMessage.trim().split(" ");
    this.command = getCommand(this.parseCommand[0], this.parseCommand.length, this.parseCommand[1]);
    this.listNumber = function getListNumber() {
        if (isNaN(this.parseCommand[1].replace('#', '').replace('delete', ''))) {
            return null;
        }
        return this.parseCommand[1].replace('#', '').replace('delete', '');
    };
    this.details = function getMessageDetails() {
        if (this.command = 'add') {
            return getCommandDetails(this.parseCommand);
        }
        return null;
    };
    this.isValid = true; //Maybe add some validation function that we can easily reference
};

//comment so it sees a change

function getCommandDetails(commandArray) {
    var text = "";
    var i;
    var what;
    what = 0;
    if (commandArray[0].toLowerCase() == 'add') {
        what = 1;
    }
    else {
        what = 0;
    }
    for (i = what; i < commandArray.length; i++) {
        text += commandArray[i] + " ";
    }
    return text.substring(0,text.length - 1)
}


function writeResults () {
    switch (testMessageTextObject.command) {
        case "add":
            return 'Okay, I will add ' + "\'" + testMessageTextObject.details() + "\'";
        case "delete":
            return "Okay, I will delete " + testMessageTextObject.listNumber;
        case "done":
            return testMessageTextObject.listNumber + " marked as done.";
        case "clear":
            return "List has been cleared.";
        default: //always assuming that we are adding
            return "Okay, I will add " + testMessageTextObject.wholeMessage;
    }
}


function getCommand(command, length, listNum) {
    switch (command.toLowerCase()) {
        case '':         //slack always adds a space after the command, but including null just in case
            return command + 'show';
        case 'show':
            if (length > 1) {
            return 'add'
            }
            return "show";
        case "delete":
            if (length == 2 && !isNaN(listNum.replace('#', '').replace('delete', ''))) {
            return 'delete'
            } else if (length == 1)
            return 'delete';
            return 'add';
        case 'done':
            if (length == 2 && !isNaN(listNum.replace('#', '').replace('done', ''))) {
                return 'done'
            } else if (length == 1)
            return 'done';
            return 'add';
        case "clear":
            if (length > 1) {
            return 'add'
            }
            return "clear";
        case "help":
            if (length > 1) {
            return 'add';
            }
            return 'help';
        case "add":
            if (length == 1) {
            return 'help'
            }
            return 'add';
        case "@feedback":
            return 'feedback';
        case "@listme":
            return 'debug';
        case "subscribe":
            if (length == 1) {
            return 'subscribe'
            }
            return 'add';
        default:
            return 'add'; //always assume we are adding something if there is not a recognized command
    }
}

module.exports = parse;