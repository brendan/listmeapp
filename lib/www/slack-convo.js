/**
 * Created by Brendan on 10/3/16.
 */

module.exports = [
    {
        person: "Zach",
        image: "zach.jpg",
        time: "5:52 pm",
        text: "See you tomorrow!"
    },
    {
        person: "Eric",
        image: "eric.jpg",
        time: "5:53 pm",
        text: "We're going to get pizza if you want to come."
    },
    {
        person: "Zach",
        image: "zach.jpg",
        time: "5:53 pm",
        text: "Sounds great.  I'll meet you over there."
    },
    {
        person: "Zach",
        image: "zach.jpg",
        time: "8:07 am",
        text: "Are you going to make this meeting?"
    },
    {
        person: "Eric",
        image: "eric.jpg",
        time: "8:07 am",
        text: "No I don't think I'm needed <br\> I'll catch up with you after that."
    },
    {
        person: "Zach",
        image: "zach.jpg",
        time: "5:52 pm",
        text: "See you tomorrow!"
    }
];