/**
 * Created by Brendan on 9/23/16.
 */
var express = require('express');
var ua = require('universal-analytics');
var visit = ua('UA-84701870-2', {https: true});
var request = require('request');
var favicon = require('serve-favicon');
var slackconvo = require('./slack-convo');
var postmark = require("postmark");
var emailclient = new postmark.Client("c26c930e-781a-465b-869b-232cc8dbedd5");

module.exports = function(app){

    app.use(express.static('public'));

    app.use(function(req,res,next){
        //global settings
        res.locals.pVersion = require('../../package.json').version;
        next()
      })

    app.get('/test-signup', function(req, res) {
        res.render('signup');
    });

    app.get('/', function (req, res) {
        visit.pageview('/').send();
        res.render('home',{
            slackitems: slackconvo,
            donthunt: "hidden"
        });
    });

    app.get('/home', function(req, res) {
        visit.pageview('/').send();
        res.render('home',{
            slackitems: slackconvo,
            donthunt: ""
        });
    });

    app.get('/home-mockup', function(req, res) {
        visit.pageview('/').send();
        res.render('homemockup',{
            slackitems: slackconvo,
            donthunt: "hidden"
        });
    });

    app.get('/feedback', function(req, res) {
        res.render("feedback");
    });

    app.post("/checkout", function(req, res) {
        visit.pageview('/checkout').send();
        request.post({
            url: 'https://' + req.get('host') + '/api/bt/addcustomer',
            body: req.body,
            json: true
        }, function(err, results) {
            if (results.statusCode == 500) {
                res.redirect("order-error");
            }
            else {
                emailclient.sendEmailWithTemplate({
                    "From": "support@listme.chat",
                    "To": req.body.email,
                    "TemplateId": 977161,
                    "TemplateModel": {
                        "product_name": "listMe",
                        "name": req.body.teamname,
                        "sender_name": "listMe Support",
                        "product_address_line1": "1505 Rochester Ct.",
                        "product_address_line2": "Crofton, MD 21114",
                        "action_url": "http://www.listme.chat",
                        "username": "listMe"
                    }
                });
                res.redirect("success?sub=yes");
            }
        });
    });

    app.get('/success', function(req, res) {
        visit.pageview('/success').send();
        res.render('success');
    });

    app.get('/sub-canceled', function(req, res) {
        visit.pageview('/sub-canceled').send();
        res.render('subcanceled');
    });

    app.get('/order-error', function(req, res) {
        visit.pageview('/order-error').send();
        res.render('ordererror');
    });

    app.get('/paypalcancel', function(req, res) {
        visit.pageview('/paypalcancel').send();
        res.render('paypalcancel');
    });

    app.get('/subscribe', function(req, res) {
        visit.pageview('/subscribe').send();
        visit.event("Subscription", "Payment landing page").send();
        res.render('subscribe', {
            planID: req.query.planID,
            cost: req.query.cost,
            team: {
                team: req.query.team,
                team_id: req.query.teamID,
                user: req.query.user,
                user_id: req.query.userID
            },
            clientT: {
                token: req.query.clientT
            }
        });
    });

    app.get('/account', function(req, res) {
        visit.pageview('/account').send();
        if (req.query.logintoken) {
            request.get({
                url: 'https://' + req.get('host') + '/api/checklogin?logintoken=' + req.query.logintoken
            }, function(err, results) {
                if (results.statusCode == 500) {
                    res.status(500).render("error", { theerror: "Something went wrong getting your account information. "})
                }
                else {
                    if (results.statusCode == 404) {
                        res.status(401).render("error", { theerror: "Sorry, I cannot find that login information.  Please login again."});
                    } else {
                        res.render('account', { info: JSON.parse(results.body) });
                    }
                }
            });
        } else {
            res.redirect('/');
        }
    });

    app.get('/help', function(req, res) {
        visit.pageview('/help').send();
        res.render("help");
    });

    app.get('/privacy', function(req, res) {
        visit.pageview('/privacy').send();
       res.render('privacy');
    });

    app.get('/terms', function(req, res) {
        visit.pageview('/terms').send();
        res.render('terms');
    });

    app.get('/beta', function(req, res) {
        visit.pageview('/beta').send();
        res.redirect("/?beta=yes");
    });

    app.get('/support', function(req, res) {
        res.redirect('/help');
    })

    app.get('/video', function(req, res) {
        visit.pageview('video');
        res.render('video');
    })
};