var Handlebars = require('handlebars');

module.exports = {
    defaultLayout: 'main',
    helpers: {
        getSubStatus: function(context, options) {
            var ret  = "Status Unknown";
            //Status: <label class="label label-danger">Canceled</label>
            /*
             braintree.Subscription.Status.Active
             braintree.Subscription.Status.Canceled,
             braintree.Subscription.Status.Expired,
             braintree.Subscription.Status.PastDue,
             braintree.Subscription.Status.Pending
            * */
            switch (context) {
                case "Active":
                    ret = 'Status: <label class="label label-primary">Active</label>';
                    break;
                case "Canceled":
                    ret = 'Status: <label class="label label-danger">Canceled</label>';
                    break;
                case "Expired":
                    ret = 'Status: <label class="label label-danger">Expired</label>';
                    break;
                case "PastDue":
                    ret = 'Status: <label class="label label-danger">Past Due</label>';
                    break;
                case "Pending":
                    ret = 'Status: <label class="label label-info">Pending</label>';
                    break;
                default:
                    ret = 'Status: <label class="label label-default">Unknown</label>'
            }
            return new Handlebars.SafeString(ret);
        },
        showCancelSub: function(context, options) {
            var fnTrue = options.fn,
                fnFalse = options.inverse;

            switch (context) {
                case "Canceled":
                    return fnFalse(this);
                default:
                    return fnTrue(this);
            }
        },
        noActiveSub: function(context, options) {
            var fnTrue = options.fn,
                fnFalse = options.inverse;
            
            let zeroSubs = true;

            if (context) {
                context.forEach((card, i) => {
                    card.subscriptions.forEach((sub, j) => {
                        if (sub.status == "Active") {
                            zeroSubs = false;
                        }
                    })
                })
            }

            if (zeroSubs) {
                return fnTrue(this);
            } else {
                return fnFalse(this);
            }
        },
        section: function(name, options){
            if(!this._sections) this._sections = {};
            this._sections[name] = options.fn(this);
            return null;
        },
        toLowerString: function(str, options) {
            return str.toString().toLowerCase();
        },
        everyOther: function(index, amount, scope) {
            if ( ++index % amount)
                return scope.inverse(this);
            else
                return scope.fn(this);
        },
        json: function(context) {
            return JSON.stringify(context);
        },
        greaterThan: function(context, options) {
            if (context > options) {
                return true;
            } else {
                return false;
            }
        }
    }
};