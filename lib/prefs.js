/**
 * Created by Brendan on 8/22/16.
 */

const subLink = "<https://slack.com/oauth/authorize?scope=commands&client_id=70109410515.83017068224|click here>"

module.exports = function(){
    this.SLtitle = 'listMe';
    this.sayHi = "One moment...";
    this.cleared = "List cleared.";
    this.itemStyle = '`';
    this.paywallSoft = '`We noticed you\'ve had more active users in the last month than our free tier allows for.`  \n\n' + `Please ${subLink} to update your subscription information.  Thank you`;
    this.donateSoft = "This week marks one year of listMe - thank you all for making it so great! We're asking anyone that feels passionate about listMe to donate to help keep the servers spinning.  Visit https://goo.gl/afRu7s"
    this.subMessage = `To subscribe to listMe, please ${subLink}`
    this.friendlyError = {
        commandNotRecognized: "I'm sorry but I didn't recognize that command.  Please try again.",
        unkCommand: "I didn't understand that command please try again.",
        general: "Something went wrong.  Please try again.",
        listNotFound: 'No list items yet.  Use "/list add *item*" to add.',
        itemDeleted: "Item deleted.",
        itemStruck: "Item marked as complete.",
        addError: "There was an error adding to the list.",
        itemNotFound: "Sorry that item was not found, please try again.",
        noDeleteNum: "Sorry, but I'm not sure which item you want to delete \n Please type \"/list delete # \" where # is the number of the item to delete",
        noStrikeNum: "Sorry, but I'm not sure which item you want to mark as done \n Please type \"/list done # \" where # is the number of the item to mark as complete"
    };
    this.helpText = "Available Commands:\n" +
                    "`/list [text you want to add]` adds an item\n" +
                    "`/list` shows the list\n" +
                    "`/list delete [# of item]` deletes the item specified\n" +
                    "`/list clear` deletes all items on the list\n" + 
                    "`/list subscribe` helps you subscribe to listMe\n" +
                    "`/list @feedback [feedback you want to add]` sends feedback\n\n" +
                    "For more information, visit <https://listme.chat/help|listme.chat/help>";

    this._featureNotAvail = function (featureName, tier) {
        return `Sorry, but ${featureName} is only availble in our \`${tier}\` tier.`
    }

    this._internalHeaders = { "Authorization": process.env.clientSecret }

    this._getListHTML = function (list) {
        var text = 'No list items yet.  Use "/list add *item*" to add.';
        if (list) {
            if (list.items) {
                if (list.items.length > 0) {
                    text = '';
                    for (i = 0; i < list.items.length; i++) {
                        text = text + (i + 1).toString() + ') ';
                        text = text + list.items[i].text + '\n';
                    }
                }
            }
        }

        return text;
    };

    this._makeURL = function(req, resource, hostor) {
        let host = 'listme.chat'
        if (!(resource.indexOf('/') == 0)) { resource = '/' + resource; }
        var http = 'https://';
        if ((process.env.NODE_ENV || 'development') === 'development') {
            http = 'http://';
            host = 'listme.serveo.net'
        }

        if (req) { host = req.get('host'); }

        let url = ''
        if (hostor) {
            url = http + hostor + resource;
        } else {
            url = http + host + resource;
        }

        return url;
    };

    this._normalizePort = function(val) {
        var port = parseInt(val, 10);
        if (isNaN(port)) {
            // named pipe
            return val;
        }
        if (port >= 0) {
            // port number
            return port;
        }
        return false;
    }
};