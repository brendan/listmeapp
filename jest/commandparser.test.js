var PARSER = require('../lib/commandparser');

var test_add        = 'add do that thing that I have been meaning to do';
var test_delete     = 'delete 1';
var test_done       = 'done 4';
var test_clear      = 'clear';
var test_random     = 'safds 7';
var test_subscribe  = 'subscribe';
var test_help       = 'help';

let answer = {};

describe("Command Parser - Basics", () => {
    beforeAll(() => {
        answer = new PARSER(test_add);
    });

    it('is truthy', () => {
        expect(PARSER).toBeDefined();
        expect(PARSER).not.toBeNull();
        expect(PARSER).toBeTruthy();
    });

    it('handles command and has correct props', () => {
        expect(answer).toBeTruthy();
    });

    it('has expected props', () => {
        expect(answer).toHaveProperty('wholeMessage');
        expect(answer).toHaveProperty('parseCommand');
        expect(answer).toHaveProperty('command');
        expect(answer).toHaveProperty('listNumber');
        expect(answer).toHaveProperty('details');
        expect(answer).toHaveProperty('isValid');
    })

    describe("Add command", () => {
        beforeAll(() => {
            answer = new PARSER(test_add);
        });
    
        it('handles add', () => {
            expect(answer.isValid).toBe(true);
            expect(answer.command).toBe('add');
            expect(answer.details()).toBe('do that thing that I have been meaning to do');
        });
    });

    describe("Delete command", () => {
        beforeAll(() => {
            answer = new PARSER(test_delete);
        });
    
        it('handles delete', () => {
            expect(answer.isValid).toBe(true);
            expect(answer.command).toBe('delete');
            expect(answer.details()).toBe('delete 1');
            expect(answer.listNumber()).toEqual("1");
        });
    });
    
    describe("Done command", () => {
        beforeAll(() => {
            answer = new PARSER(test_done);
        });
    
        it('handles done', () => {
            expect(answer.isValid).toBe(true);
            expect(answer.command).toEqual('done');
            expect(answer.details()).toEqual('done 4');
            expect(answer.listNumber()).toEqual("4");
        });
    });
    
    describe("Clear command", () => {
        beforeAll(() => {
            answer = new PARSER(test_clear);
        });
    
        it('handles clear', () => {
            expect(answer.isValid).toBe(true);
            expect(answer.command).toBe('clear');
            expect(answer.details()).toBe('clear');
        });
    });
    
    describe("Subscribe command", () => {
        beforeAll(() => {
            answer = new PARSER(test_subscribe);
        });
    
        it('handles clear', () => {
            expect(answer.isValid).toBe(true);
            expect(answer.command).toBe('subscribe');
            expect(answer.details()).toBe('subscribe');
        });
    });

    describe("Help command", () => {
        beforeAll(() => {
            answer = new PARSER(test_help);
        });
    
        it('handles clear', () => {
            expect(answer.isValid).toBe(true);
            expect(answer.command).toBe('help');
            expect(answer.details()).toBe('help');
        });
    });

    describe("Check debug command", () => {
        beforeAll(() => {
            answer = new PARSER("@listMe feature strikeItem");
        });
    
        it('handles debug', () => {
            expect(answer.isValid).toBe(true);
            expect(answer.command).toBe('debug');
            expect(answer.details()).toBe('@listMe feature strikeItem');
        });
    });

    describe("Random (no command)", () => {
        beforeAll(() => {
            answer = new PARSER(test_random);
        });
    
        it('handles add', () => {
            expect(answer.isValid).toBe(true);
            expect(answer.command).toBe('add');
            expect(answer.details()).toBe('safds 7');
        });
    });
    
    describe("Common use cases", () => {
        it("handles Mr Robot", () => {
            answer = new PARSER("Mr Robot");
            expect(answer.command).toBe('add');
            expect(answer.details()).toEqual("Mr Robot");
        });

        it("handles add Mr Robot", () => {
            answer = new PARSER("add Mr Robot");
            expect(answer.command).toBe('add');
            expect(answer.details()).toEqual('Mr Robot')
        })
    })
});

