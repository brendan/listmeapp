var PARSER = require('../lib/commandparser');

let phrase = ''
let answer = {}

describe("Command Parser - Edge cases", () => {  
    describe("Delete with hashtag before list nubmer", () => {
        beforeAll(() => {
            phrase = "delete #3" 
            answer = new PARSER(phrase)
        })
        
        it('identifies as delete command', () => {
            expect(answer.command).toEqual("delete")
        })
        it('correctly finds the list number', () => {
            expect(answer.listNumber()).toEqual("3")
        })
    });
    
    describe("Starts with `HELP` but is an item", () => {
        it('correctly adds the item', () => {
            phrase = "help Brendan something, then do other things"
            answer = new PARSER(phrase)
            expect(answer.isValid).toBe(true);
            expect(answer.command).toEqual("add")
            expect(answer.details()).toEqual(phrase)
        })
    })

    describe("Starts with `DELETE` but is an item", () => {
        it('correctly adds the item', () => {
            phrase = "delete something, then do other things"
            answer = new PARSER(phrase)
            expect(answer.isValid).toBe(true);
            expect(answer.command).toEqual("add")
            expect(answer.details()).toEqual(phrase)
        })
    })
    
    describe("Starts with `DELETE` and a number but is an item", () => {
        it('correctly adds the item', () => {
            phrase = "delete 1 something, then do other things"
            answer = new PARSER(phrase)
            expect(answer.isValid).toBe(true);
            expect(answer.command).toEqual("add")
            expect(answer.details()).toEqual(phrase)
        })
    })
    
    describe("Odd command: starts with `DONE` but is an item", () => {
        it('correctly adds the item', () => {
            phrase = "done with things, then do other things"
            answer = new PARSER(phrase)
            expect(answer.isValid).toBe(true);
            expect(answer.command).toEqual("add")
            expect(answer.details()).toEqual(phrase)
        })
    })
    
    describe("Starts with `DONE` and a number but is an item", () => {
        it('correctly adds the item', () => {
            phrase = "done 1 something, then do other things"
            answer = new PARSER(phrase)
            expect(answer.isValid).toBe(true);
            expect(answer.command).toEqual("add")
            expect(answer.details()).toEqual(phrase)
        })
    })

    describe("Starts with `SUBSCRIBE` but is an item", () => {
        it('correctly adds the item', () => {
            phrase = "subscribe to all things, then do other things"
            answer = new PARSER(phrase)
            expect(answer.isValid).toBe(true);
            expect(answer.command).toEqual("add")
            expect(answer.details()).toEqual(phrase)
        })
    })
});

