const redis    = require("redis");

const mockResponse = function Constructor() {
    const res = {};
    res.status      = jest.fn().mockReturnValue(res).mockName("mockResStatus");
    res.json        = jest.fn().mockReturnValue(res).mockName("mockResJson");
    res.send        = jest.fn().mockReturnValue(res).mockName("mockResSend");
    res.sendStatus  = jest.fn().mockReturnValue(res).mockName("mockResSendStatus");
    return res;
};

const mockRequest = function Constructor(authHeader, sessionData) {
    const req = {
        get(name) {
            if (name === 'authorization') return authHeader
            return null
        },
        header(name) {
            if (name === 'Authorization') return authHeader
            return null
        },
        session: { data: sessionData }
    };
    return req;
}

module.exports = {
    databaseClient: jest.genMockFromModule('redis').createClient(),
    apiLog: jest.fn((msg, caller, req) => { return true; }),
    express: {
        res: mockResponse,
        req: mockRequest
    }
}