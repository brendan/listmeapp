API documentation is available at `/doc`

To regenerate the API documentation run:

1. `npm install apidoc -g`
1. `apidoc -i lib -o doc/`