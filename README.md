# listMe from Liscio Apps

> Keep lists of to-dos, discussion topics, or ideas with each person you chat with in Slack!

## DEV 

* Run `npm run dev` (will runn app and serveo with foreman)
* Homepage:         https://listme.serveo.net/
* Slash command:    `/test_list`

## TEST :white_check_mark:
* Homepage:                 https://listme-test.herokuapp.com/
* Test app:                 https://dashboard.heroku.com/apps/listme-test 
* Payment sandbox:          https://sandbox.braintreegateway.com/login (login direct)
* Slash command:            `/test_list`
* Slack for testing:        https://olearycrew.slack.com

## PRODUCTION :rocket:
* Homepage:                 https://listme.chat/
* Admin:                    https://listme.chat/admin
* Slack App Store listing:  https://slack.com/apps/A2F0H206L-listme
* PRODUCTION app:           https://dashboard.heroku.com/apps/slacklists 
* Payment processing PROD:  https://www.braintreegateway.com/login (login with PayPal)
* Slash command:            `/test`

## Development Setup :zap:

### Local Development :construction_worker:

1. Clone this repository
1. `npm install`
1. Create a new branch with `git checkout -b 99-your-branch` where `99` is your issue number
1. Create a `.env` file from the `.env.example` file.
1. Fill in the test parts of the `.env` file wit hthe correct credientials 
1. Run `npm start` to run 
1. Open http://localhost:5000/ 

#### Proxy to local dev server

1. Log in to https://dashboard.ngrok.com
1. Download & install `NGROK`
1. Run the command to install the authtoken to your ngrok install
1. Start the dev server with `npm start`
1. In a second terminal, run `ngrok http 5000`
1. Copy the `https` URL to the [TEST ListMe Slack dashboard](https://api.slack.com/apps/AG3S357B9/slash-commands)
1. Run `/test_list` or do the other stuff you want
1. :tada:


### Deploy to TEST :truck:

1. `heroku login`
1. Go through login process
1. `heroku git:remote -a listme-test`
1. `git push heroku master`
1. :tada:

## API Documentation :building_construction:

See [API.md](API.md)

## Heroku Standup

See [setup/Heroku.md](./setup/Heroku.md)

## Slack Standup

See [setup/Slack.md](./setup/Slack.md)

## Database Documentation

See [setup/Database.md](./setup/Database.md)

## Contributing

Contact us at www.liscioapps.com

## License

All rights reserved ©2015-present Liscio Apps
