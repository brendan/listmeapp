/**
 * Created by Brendan on 9/9/16.
 */
var prereq = require('./lib/prereq');
var slacklist = new prereq();
var prefs = new slacklist.prefs();

slacklist.webApp.get('/', function (req, res) {
    res.send('Hello World!');
});

require('./lib/api')(slacklist.webApp, slacklist.redis);

slacklist.webApp.post("/foofoo", function(req, res) {
    console.log(req.body.team_id);
    showList(req, res);
});

var port = prefs._normalizePort(process.env.PORT || '3000');

slacklist.webApp.listen(port, function () {
    slacklist._log('App listening on port ' + port);
});



function showList(req, res) {
    var options = {
        url:  prefs._makeURL(req, '/api/team/' + req.body.team_id + '/lists/' + req.body.channel_id),
        json: true,
        body: { token: req.body.token }
    };

    slacklist.request.get(options, function(err, response, body) {
        if (err || !(response.statusCode == 200)) {
            if (err) { slacklist._error(err); }
            if (response.statusCode == 404) {
                res.send(prefs.friendlyError.listNotFound);
            }
            else {
                res.send(prefs.friendlyError.general);
            }
        }
        else {
            res.send({
                response_type: "in_channel",
                text: prefs._getListHTML(body)
            });
        }
    });
}