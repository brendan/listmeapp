#!/bin/bash

echo "Running serveo on port $TEST_PORT for $TEST_DOMAIN"
ssh -R $TEST_DOMAIN.serveo.net:80:localhost:$TEST_PORT serveo.net