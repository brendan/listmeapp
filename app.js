/**
 * Created by Brendan on 8/22/16.
 */
 

require('dotenv').config();

var prereq = require('./lib/prereq');
var slacklist = new prereq();
var prefs = new slacklist.prefs();
const debugTools = require('./lib/debug')

var cron = require('node-cron');

require('./lib/api')(slacklist, slacklist.redis);
require('./lib/www')(slacklist.webApp);
require('./lib/admin')(slacklist, slacklist.redis);
const tierHandler = require('./lib/helpers/tierHandlers');

console.log("Such change!")

cron.schedule('*/15 * * * *', () => {
    let msg = 'Running paywall update (every 15 minutes)'
    slacklist._log(msg);
    slacklist.logger.info(msg, { caller: "cron.schedule.1", details: "calling updatePaying()" })
    updatePaying();
});

slacklist.webApp.post('/app/postdata', function (req, res) {
    res.send(prefs.sayHi);
    slacklist.logger.info("New post to /app/postdata", { caller: 'slacklist.webApp.post', details: req.body })
    var rURL = req.body.response_url;
        if (req.body.command == '/list' || req.body.command == '/devlist' || req.body.command == '/test_list') {
            listCommand(req, res);
            teamStatRecord(req);
        }
        else {
            slacklist._error(prefs.friendlyError.commandNotRecognized + "(" + req.body.command + ")");
            slacklist.slack.send(rURL, prefs.friendlyError.commandNotRecognized);
        }
});

function listCommand(req, res) {
    var com = new slacklist.comparser(req.body.text);
    switch (com.command) {
        case "add":
            slacklist.logger.info("Add command", { command: "add", caller: 'listCommand', details: req.body })
            addToList(req, com.details());
            break;
        case "delete":
            slacklist.logger.info("Delete command", { command: "delete", caller: 'listCommand', details: req.body })
            deleteFromList(req, com.listNumber(), true);
            break;
        case 'done':
            slacklist.logger.info("Done command", { command: "done", caller: 'listCommand', details: req.body })
            doneItem(req, com.listNumber(), true);
            break;
        case "clear":
            slacklist.logger.info("Clear command", { command: "clear", caller: 'listCommand', details: req.body })
            clearList(req);
            break;
        case "show":
            slacklist.logger.info("Show command", { command: "show", caller: 'listCommand', details: req.body })
            showList(req, res);
            break;
        case "feedback":
            slacklist.logger.info("Feedback command", { command: "feedbak", caller: 'listCommand', details: req.body })
            feedback(req, com.details());
            break;
        case "debug":
            slacklist.logger.info("Debug command", { command: "debug", caller: 'listCommand', details: req.body })
            debugTools.processCommand(req, com.details());
            break;
        case "unknown command":
            slacklist.slack.send(req.body.response_url, prefs.friendlyError.unkCommand);
            break;
        case "help":
            slacklist.slack.send(req.body.response_url, prefs.helpText);
            break;
        case "subscribe":
            slacklist.slack.send(req.body.response_url, prefs.subMessage);
            break;
        default:
            slacklist.slack.send(req.body.response_url, prefs.friendlyError.general);
            break;
    }
}

function teamStatRecord(req) {
    var options = {
        url:  prefs._makeURL(req, '/api/teams/' + req.body.team_id + '/users'),
        json: true, body: req.body, 
        headers: prefs._internalHeaders
    };
    //Add user
    slacklist.request.post(options, function(err, response, body) {
        if (err) { slacklist._error(err); }
    });

    //Check team info
    options.url = prefs._makeURL(req, '/api/teams');
    slacklist.request.post(options, function(err, response, body) {
        if (err) { slacklist._error(err); }
    });

    //Yell if they aren't paying and they have exceeded the user limit.
    paywall(req);
}

function updatePaying() {
    let host = 'listme.chat'
    if ((process.env.NODE_ENV || 'development') === 'development') {
        host = 'localhost:5000'
    }

    var options = {
        url:   prefs._makeURL(null, '/api/paywall/update', host),
        headers: prefs._internalHeaders
    };

    slacklist.request.get(options, function(err, response, body) {
        if (err) { slacklist._error(err); } else {
            slacklist._log("UPDATED PAYING CUSTOMERS");
            slacklist.logger.info("UPDATED PAYING CUSTOMERS", {
                caller: "updatePaying",
                details: body
            })
        }
    });

    options.url = prefs._makeURL(null, '/api/paywall/violating', host);

    slacklist.request.get(options, function(err, response, body) {
        if (err) { slacklist._error(err); } else {
            slacklist._log("UPDATED VIOLATING CUSTOMERS");
            slacklist.logger.info("UPDATED VIOLATING CUSTOMERS", {
                caller: "updatePaying",
                details: body
            })
        }
    })
}

function paywall(req) {
    var options = {
        url:   prefs._makeURL(req, '/api/teams/nonpaying'),
        json: true, body: req.body,
        headers: prefs._internalHeaders
    };
    slacklist.request.get(options, function(err, response, body) {
        if (err) { slacklist._error(err); } else {
            var nonpaying = body;
            var team = req.body.team_id;
                if (nonpaying.indexOf(team) > -1) {
                    console.log("PAYWALL | Message sent to " + req.body.team_domain);
                    slacklist.logger.info(`PAYWALL | Message sent to ${req.body.team_domain}`, {
                        team: req.body.team_domain,
                        teamid: req.body.team_id,
                        message: prefs.paywallSoft,
                        caller: 'paywall(req)'
                    });
                    slacklist.slack.send(req.body.response_url, prefs.paywallSoft, true);
                }
        }
    });
}


function addToList(req, item) {
    var user = req.body.user_name;
    var startofphrase = "I added ";
    if (user) { startofphrase = '@' + user + " added "; }
    var rURL = req.body.response_url;
    var reqbody = req.body;
    var postdata = {
        team_id: reqbody.team_id,
        list_id: reqbody.channel_id,
        name: item,
        token: reqbody.token
    };

    var options = {
        url:  prefs._makeURL(req, '/api/team/' + postdata.team_id + '/lists/' + postdata.list_id),
        json: true,
        body: postdata, 
        headers: prefs._internalHeaders
    };

    slacklist.request.post(options, function(err, response, body) {
        if (err) {
            slacklist._error(err);
            slacklist.slack.send(rURL, prefs.friendlyError.addError);
        }
        else {
            slacklist.slack.send(rURL, startofphrase + prefs.itemStyle + body.name + prefs.itemStyle + ' to the list.', true);
        }
    });
}

function deleteFromList(req, num, talk) {
    var rURL = req.body.response_url;

    if (num) {
        var options = {
            url:  prefs._makeURL(req, '/api/team/' + req.body.team_id + '/lists/' + req.body.channel_id + '/item/' + num.toString()),
            json: true,
            body: { token: req.body.token },
            headers: prefs._internalHeaders
        };

        slacklist.request.delete(options, function(err, response, body) {
            if (err && response.status != 404) {
                slacklist._error(err);
                slacklist.slack.send(rURL, prefs.friendlyError.general);
            }
            else {
                if (talk) {
                    if (response.statusCode == 404) {
                        slacklist.slack.send(rURL, prefs.friendlyError.itemNotFound); }
                    else {
                        slacklist.slack.send(rURL, prefs.friendlyError.itemDeleted); }
                }
            }
        });
    }
    else { slacklist.slack.send(rURL, prefs.friendlyError.noDeleteNum); }
}

function doneItem(req, num, talk) {
    const featName = "strikeItem";

    slacklist.logger.info(`Done item feature used`, {
        feature: featName,
        caller: 'doneItem'
    })
    tierHandler.check(req, num, talk, featName, strike);
}

function strike(req, num, talk, allowed) {
    slacklist.tierLog("strikeItem", "strike", allowed, req);

    if (allowed) {
        strikeFromList(req, num, talk);
    } else {
        var rURL = req.body.response_url;
        slacklist.slack.send(rURL, prefs._featureNotAvail("strike-through on items", "Standard"));
        setTimeout(() => {
            slacklist.slack.send(rURL, prefs.subMessage);
        }, 1000);
    }
}

function strikeFromList(req, num, talk) {
    var rURL = req.body.response_url;

    if (num) {
        var options = {
            url:  prefs._makeURL(req, '/api/team/' + req.body.team_id + '/lists/' + req.body.channel_id + '/item/' + num.toString()),
            json: true,
            body: { token: req.body.token },
            headers: prefs._internalHeaders
        };

        slacklist.request.post(options, function(err, response, body) {
            if (err && response.status != 404) {
                slacklist._error(err);
                slacklist.slack.send(rURL, prefs.friendlyError.general);
            }
            else {
                if (talk) {
                    if (response.statusCode == 404) {
                        slacklist.slack.send(rURL, prefs.friendlyError.itemNotFound); }
                    else {
                        slacklist.slack.send(rURL, prefs.friendlyError.itemStruck); 
                    }
                }
            }
        });
    }
    else { slacklist.slack.send(rURL, prefs.friendlyError.noStrikeNum); }
}

function clearList(req) {
    var rURL = req.body.response_url;
    var options = {
        url:  prefs._makeURL(req, '/api/team/' + req.body.team_id + '/lists/' + req.body.channel_id),
        json: true,
        body: { token: req.body.token, response_url: req.body.response_url },
        headers: prefs._internalHeaders
    };

    slacklist.request.delete(options, function(err, response, body) {
        if (err || !(response.statusCode == 404) ){
            slacklist.slack.send(rURL, prefs.friendlyError.general);
        } else {
            slacklist.slack.send(rURL, prefs.cleared, true);
        }
    })
}

function showList(req) {
    var rURL = req.body.response_url;
    var options = {
        url:  prefs._makeURL(req, '/api/team/' + req.body.team_id + '/lists/' + req.body.channel_id),
        json: true,
        body: { token: req.body.token, response_url: req.body.response_url },
        headers: prefs._internalHeaders
    };

    slacklist.request.get(options, function(err, response, body) {
        if (err || !(response.statusCode == 200)) {
            if (err) { slacklist._error(err); }
            if (response.statusCode == 404) {
                slacklist.slack.send(rURL, prefs.friendlyError.listNotFound);
            }
            else {
                slacklist.slack.send(rURL, prefs.friendlyError.general);
            }
        }
        else {
            slacklist.slack.send(rURL, {response_type: "in_channel",text: prefs._getListHTML(body)});
        }
    });
}

function feedback(req, FB) {
    var rURL = req.body.response_url;
    var user = req.body.user_id;
    var user = req.body.team_id + "_" + user;
    var options = {
        url:  prefs._makeURL(req, '/api/feedback'),
        json: true,
        body: { name: FB, token: req.body.token, response_url: req.body.response_url, user: user },
        headers: prefs._internalHeaders
    };

    slacklist.request.post(options, function(err, response, body) {

    });
}

var port = prefs._normalizePort(process.env.PORT || '3000');

if (typeof module !== 'undefined' && module.exports != null) {
    exports.listCommand = listCommand;
    exports.SlackIT = slacklist.slack.send;
    exports.teamStatRecord = teamStatRecord;
    exports.paywall = paywall;
    exports.addToList = addToList;
    exports.deleteFromList = deleteFromList;
    exports.doneItem = doneItem;
    exports.clearList = clearList;
    exports.showList = showList;
    exports.feedback = feedback;
}