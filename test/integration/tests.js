/**
 * Created by Brendan on 10/1/16.
 */
var newman = require('newman'); // require newman in your project
var request = require('request');

// call newman.run to pass `options` object and wait for callback
newman.run({
    collection: require('./RESTtests.json'),
    reporters: 'json',
    iterationCount: 5,
    delayRequest: 100
}, function (err, summary) {
    var theURL = 'https://hooks.slack.com/services/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';
    if (err) {
        request.post({
            url: 'https://hooks.slack.com/services/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',
            body: {
                text: "There was an error running the tests!"
            },
            json: true
        });

        throw err;
    } else {
        //colors.  red: #a94442, green: #36a64f
        var success = true;
        var fall = "PASSED";
        if (summary.run.failures.length > 0 || summary.run.errors) {
            success = false;
            fall = "FAILED";
        }

        var color = "#000000";
        var titleText = "Automated Testing Run: " + fall;

        if (success) {
            color = "#36a64f";
        } else {
            color = "#a94442";
        }

        var msg = {
            attachments: [{
                fallback: fall,
                color: color,
                title: titleText,
                fields: [
                    {
                        title: "Iterations",
                        value: summary.run.stats.iterations.total,
                        short: true
                    },
                    {
                        title: "Tests",
                        value: summary.run.stats.tests.total,
                        short: true
                    },
                    {
                        title: "Total Assertions",
                        value: summary.run.stats.assertions.total,
                        short: true
                    },
                    {
                        title: "Failing Assertations",
                        value: summary.run.stats.assertions.failed,
                        short: true
                    },
                    {
                        title: "Average Response Time (ms)",
                        value: Math.round(summary.run.timings.responseAverage),
                        short: true
                    }
                ],
                footer: "Automated testing from tests/tests.js"
            }]
        };
        
        request.post({
            url: 'https://hooks.slack.com/services/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',
            body: msg,
            json: true
        }, function(err, result) {
            //console.log(err);
            //console.log(result.body);
        });

        if (summary.run.stats.assertions.failed > 0){
            console.log(summary.run.failures);
        }

        console.info('collection run complete!');
    }
});