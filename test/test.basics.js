var assert = require('chai').assert;
var app = require('../app.js');

describe('Mocha\'s basic array', function() {
  describe('#indexOf()', function() {
    it('should return -1 when the value is not present', function() {
      assert.equal(-1, [1,2,3].indexOf(4));
    });
  });
});

describe('Slacklist App', function() {
  describe('initialize', function() {
    it('should create the app object', function() {
      assert.isOk(app);
    });

    it('should have the listCommand method', function() {
      assert.isOk(app.listCommand);
    });

    it('should have the SlackIT method', function() {
      assert.isOk(app.SlackIT);
    });

    it('should have the teamStatRecord method', function() {
      assert.isOk(app.teamStatRecord);
    });

    it('should have the paywall method', function() {
      assert.isOk(app.paywall);
    });

    it('should have the addToList method', function() {
      assert.isOk(app.addToList);
    });

    it('should have the deleteFromList method', function() {
      assert.isOk(app.deleteFromList);
    });

    it('should have the doneItem method', function() {
      assert.isOk(app.doneItem);
    });

    it('should have the clearList method', function() {
      assert.isOk(app.clearList);
    });

    it('should have the showList method', function() {
      assert.isOk(app.showList);
    });

    it('should have the feedback method', function() {
      assert.isOk(app.feedback);
    });

    it('should NOT have a trucks method', function() {
      assert.isOk(!(app.trucks));
    })
  })
})
