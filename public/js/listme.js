/**
 * Created by Brendan on 10/3/16.
 *
 * <li class="media">
 <div class="media-left">
 <a href="#">
 <img class="media-object img-responsive" width="100%" src="img/{{person}}.jpg" alt="Zach profile picture">
 </a>
 </div>
 <div class="media-body">
 <h4 class="media-heading">{{person}} <span class="text-muted">{{time}}</span> </h4>
 <div class="slackmsg">
 {{text}}
 </div>
 </div>
 </li>
 */

function slackItemText(person, image, text, time) {
    var ret = "";
    ret += '<li class="media">';
    ret += '<div class="media-left"><a href="#">';
    ret += '<img class="media-object img-responsive" width="100%" src="img/{{image}}" alt="{{person}}">';
    ret += '</a>';
    ret += '</div>';

    ret += '<div class="media-body">';
    ret += '<h4 class="media-heading">{{person}} <span class="text-muted">{{time}}</span> </h4>';
    ret += '<div class="slackmsg">';
    ret += '{{text}}';
    ret += '</div>';
    ret += '</div>';
    ret += '</li>';

    ret = ret.replace(new RegExp("{{person}}", 'g'), person);
    ret = ret.replace(new RegExp("{{image}}", 'g'), image.toLowerCase());
    ret = ret.replace(new RegExp("{{time}}", 'g'), time);
    ret = ret.replace(new RegExp("{{text}}", 'g'), text);

    return ret;
}