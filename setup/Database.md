# Database Details

## Connecting to Redis
1. Open the app on Heroku
1. Go to the Redis add-on
1. Get the database credentials
1. Copy the full "URL"
1. `redis-cli -u urifromheroku`

## olearycrew Team ID
`T2237C2F5`

## Command <--> Type reference
### Type: Set
* `SMEMBERS key` - get all members of the set

### Type: Hash
* `HGETALL key` - get everything
* `HGET key field` - git a field

## Important Schema items

| Item | Type | Notes |
| ------- |------- | ------- | 
| listoflists | set |  |


### Team Data
| Item | Type | Notes |
| ------- |------- | ------- | 
| team:`teamid`:details | hash | ``` { "slackTeamName": "olearycrew" } ``` |

`TODO: Add these`

* team:`id`:list:`listid`:nextID
* team:`id`:list:`listid`

### Stats Data

`TODO: Add these`
* "stats:messages:2019:02:09"
* "stats:items:runninglist"
* "team:T2237C2F5:list:D2256FSH0:nextID"
* "stats:items:alltimescore"
* "stats:DailyActiveTeams:2019:02:09"
