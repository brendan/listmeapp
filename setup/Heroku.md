## Heroku Standup

### Required Environment Variables
For development, see `.env.example`.  This documents the required variables to stand up the app in Heroku

| Variable   | Source    |  Notes       |
| -------- | -------- | -------- |
| `btmerchantId`   | Braintree |   |
| `btprivateKey`   | Braintree |   |
| `btpublicKey`    | Braintree |   |
| `clientId`       | Slack App |   |
| `clientSecret`   | Slack App |   |
| `ERROR_PAGE_URL` | Dropbox   |  Currently broken :frown: |
| `GITLAB_TOKEN`   | GitLab    |  To create issues |
| `MAINTENANCE_PAGE_URL` | Dropbox |  Currently broken :frown: |
| `NEW_RELIC_APP_NAME` | New Relic | Prod: listMe |
| `NEW_RELIC_LICENSE_KEY` | New Relic |   |
| `NEW_RELIC_LOG` | New Relic |  Prod: stdout |
| `NEW_RELIC_NO_CONFIG_FILE` | New Relic |  Prod: true |
| `PAPERTRAIL_API_TOKEN` | Papertrail |   |
| `REDIS_URL` | Heroku |   |