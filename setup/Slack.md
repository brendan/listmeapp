## Slack Standup
### Basic Information
* PROD app name: listMe
* Short desc: `Keep lists of to-dos, discussion topics, or ideas with each person you chat with in Slack!`
* Long desc:
``` 
Keep lists of to-dos, discussion topics, or ideas with each person you chat with in Slack. Simple and easy to use, listMe is the Slack app you've been waiting for.

Never forget what you need to discuss with other team members. 

No sign-up required - just a Slack account! Free for small teams (less than 10 users), all tiers include a 30 day free trial!
```

### Slash Commands

| Item     | Setting     |
| -------- | -------- |
| Command | `/test` |
| Request URL | `{siteroot}/app/postdata` |
| Short Description | `Create and manage lists` |
| Usage Hint | `[add item] [delete #] [help]` |

### Redirect URLs
* `{siteroot}/oauth`
* `{siteroot}/api/slacksignin`
* `www.{siteroot}/oauth`
* `www.{siteroot}/api/slacksignin`

### Permission Scopes
| Item     | REASON FOR USING SCOPE     |
| -------- | -------- |
| `commands`: Add slash commands and add actions to messages (and view related content) | listMe creates lists in any slack channel by invoking the /list command. |
| `identity.basic ` Confirm user’s identity | For use with "Sign in with Slack" |

**NOTE:** Only add the identity. scopes here if you are about to submit your app to the App Directory for review, and you know what you are doing.

### UserID Translation
Translate Global IDs?  `TRUE`